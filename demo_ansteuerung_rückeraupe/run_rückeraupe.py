import sys
sys.path.append("..")
from gSFL.MiniTractor import MiniTractor,MiniTractorController 
import logging
from bricknil import start
import time 
import threading
import logging 

hub = ""

async def system():
    global hub
    hub = MiniTractorController("mini_tractor_controller", True)



if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    threading.Thread(target=start, args=(system,)).start()
    time.sleep(4)
    rueckeraupe = MiniTractor.start(bluetooth_controller=hub).proxy()
    rueckeraupe.run_forever()


