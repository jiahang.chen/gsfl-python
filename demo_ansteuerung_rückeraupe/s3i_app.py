from s3i.messages import *
from s3i.broker import *
from s3i.identity_provider import *
import uuid
import ast


CLIENT_ID = "s3i:da36496a-d2a8-411f-a01c-978e5e59d78a"
CLIENT_SECRET = "452d9a83-baa5-47bd-a01e-b437bfbe08f6"
ENDPOINT = "s3ib://s3i:da36496a-d2a8-411f-a01c-978e5e59d78a"

RECEIVER_ID = "s3i:847dc67e-9dad-4415-8e58-819b724a1a8f"
RECEIVER_ENDPOINT = "s3ibs://s3i:847dc67e-9dad-4415-8e58-819b724a1a8f"


class bcolors:
    """colors for the console log"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def callback(ch, method, properties, body):
    body_json = ast.literal_eval(body.decode('utf-8'))
    message_type = body_json.get("messageType")
    print("[S³I]: we have got a {0}{1}{2}".format(bcolors.UNDERLINE, message_type, bcolors.ENDC))

    if message_type == "serviceReply":
        print("[S³I]: {0}{1}{2}".format(bcolors.OKGREEN, body_json.get("results"), bcolors.ENDC))
    elif message_type == "getValueReply":
        print("[S³I]: {0}{1}{2}".format(bcolors.OKGREEN, body_json.get("value"), bcolors.ENDC))
    ch.stop_consuming()


def prepare_service_request():
    available_services = ["fml40::AcceptsMoveCommands", "ml40::ProvidesOperationalData",
                          "fml40::AcceptsWinchCommands", "fml40::AcceptsShieldCommands"]

    print("[S³I]: Following services are available: {0}{1}{2} ".format(bcolors.UNDERLINE, available_services,
                                                                       bcolors.ENDC))
    service_type = input("[S³I]: Please enter one of these services: ")

    if service_type not in available_services:
        print("[S³I]: service {0}{1}{2} is not available".format(bcolors.FAIL, service_type, bcolors.ENDC))
        return None

    elif service_type == "fml40::AcceptsMoveCommands":
        direction = input("[S³I]: Please enter the direction to move(0-360 grad): ")
        speed_level = input("[S³I]: Please enter the speed level (0-1): ")
        parameters = {"direction": direction, "speed_level": speed_level}

    elif service_type == "fml40::AcceptsWinchCommands":
        operation = input("[S³I]: Please enter the operation for winch (rollUp or rollDown): ")
        speed_level = input("[S³I]: Please enter the speed level (0-1): ")
        parameters = {"operation": operation, "speed_level": speed_level}

    elif service_type == "fml40::AcceptsShieldCommands":
        operation = input("[S³I]: Please enter the operation for shield (shieldUp or shieldDown): ")
        speed_level = input("[S³I]: Please enter the speed level (0-1): ")
        parameters = {"operation": operation, "speed_level": speed_level}

    elif service_type == "ml40::ProvidesOperationalData":
        print("[S³I]: Preparing message to get optional production data")
        parameters = {}


    service_request = ServiceRequest()
    service_request.fillServiceRequest(senderUUID=CLIENT_ID,
                                       receiverUUID=RECEIVER_ENDPOINT,
                                       sender_endpoint=ENDPOINT,
                                       serviceType=service_type,
                                       parameters=parameters,
                                       msgUUID="s3i:" + str(uuid.uuid4())
                                       )
    return service_request.msg.__str__()


def prepare_get_value_request():
    print("[S³I]: You can get the current value of Mini Tractor with these paths: ")
    print("Longitude: features/id2/properties/longitude")
    print("Latitude: features/id2/properties/latitude")
    print("Orientation: features/id2/properties/orientation")
    print("Current length of winch: features/id3/properties")

    print("Current speed of left motor: features/id4/properties")
    print("Current speed of right motor: features/id5/properties")
    print("Current lift of shield: features/id6/properties")
    attribute_path = input("[S³I]: Please enter a attribute path: ")
    get_value_request = GetValueRequest()
    get_value_request.fillGetValueRequest(senderUUID=CLIENT_ID,
                                          receiverUUID=[RECEIVER_ENDPOINT],
                                          sender_endpoint=ENDPOINT,
                                          attributePath=attribute_path,
                                          msgUUID="s3i:" + str(uuid.uuid4()))
    return get_value_request.msg.__str__()



if __name__ == "__main__":

    print("[S³I]: {0} Welcome to demo Mini Tractor gSFL {1}".format(bcolors.WARNING, bcolors.ENDC))

    idp = IdentityProvider(grant_type="client_credentials",
                           identity_provider_url="https://idp.s3i.vswf.dev/",
                           realm="KWH",
                           client_id=CLIENT_ID,
                           client_secret=CLIENT_SECRET
                           )

    access_token = idp.get_token(TokenType.ACCESS_TOKEN)
    broker = Broker(auth_form="Username/Password", username="",
                    password=access_token,
                    host="rabbitmq.s3i.vswf.dev")

    run = True

    while run:
        print("[S³I]: You can send following messages to mini tractor by entering 1 or 2")
        request_type = input(" {0}[1]: service request, [2]: get value request:{1} ".format(
            bcolors.OKBLUE, bcolors.ENDC
        ))

        if request_type == "1":
            msg = prepare_service_request()
            if msg is None:
                continue

        elif request_type == "2":
            msg = prepare_get_value_request()
            if msg is None:
                continue

        else:
            continue

        broker.send(receiver_endpoints=[RECEIVER_ENDPOINT], msg=msg)

        broker.receive(queue=ENDPOINT, callback=callback)
