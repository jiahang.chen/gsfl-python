FORWARDER_A_MODEL = {
    "thingId": "s3i:c81c7f54-46f4-40d6-a6e6-4bd0e8a7f08d",
    "policyId": "s3i:c81c7f54-46f4-40d6-a6e6-4bd0e8a7f08d",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "fml40::Forwarder"}],
        "name": "DT of Forwarder A",
        "features": [
            {
                "class": "fml40::Forwards",
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature: id1",
                "latitude": "ditto-feature: id2"
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "longitude": ""
            }
        },
        "id2": {
            "properties": {
                "latitude": ""
            }
        }
    }
}


GRANT_TYPE = "client_credentials"
CLIENT_ID = "s3i:c81c7f54-46f4-40d6-a6e6-4bd0e8a7f08d"
CLIENT_SECRET = "1d38a077-22e2-4ca5-ab08-81f5a76d4fb4"
ENDPOINT = "s3ib://s3i:c81c7f54-46f4-40d6-a6e6-4bd0e8a7f08d"
LONGITUDE_PATH = "features/id1/properties/longitude"
LATITUDE_PATH = "features/id2/properties/latitude"
REPO_TOPIC = "s3i/c81c7f54-46f4-40d6-a6e6-4bd0e8a7f08d/things/twin/commands/modify"
