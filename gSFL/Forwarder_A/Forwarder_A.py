from ..BaseThing import BaseThing
from s3i.messages import *
import uuid
from .Config import *
from modelling_language.fml40 import Forwards


class ForwarderA(BaseThing):
    def __init__(self):
        super(ForwarderA, self).__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            endpoint=ENDPOINT,
            fml40_data_model=FORWARDER_A_MODEL,
            grant_type=GRANT_TYPE,
            is_broker=True,
            is_repo=True
        )
        Forwards.start(ref_managing_actor=self.actor_ref, name="fml40::Forwards")

    def on_service_request(self, msg):
        service_reply = ServiceReply()
        request_sender = msg.get("sender")
        request_msg_id = msg.get("identifier")
        request_reply_endpoint = msg.get("replyToEndpoint")
        request_service_type = msg.get("serviceType")
        parameters = msg.get("parameters")
        reply_uuid = "s3i:" + str(uuid.uuid4())
        if request_service_type not in self.registeredServices:
            results = {"availableServiceType": self.registeredServices}

        elif request_service_type == "fml40::Forwards":
            job = parameters.get("job")
            self.proxyFunctionalities["fml40::Forwards"].executeJob(job=job)
            results = {"Forwarding": True}

        service_reply.fillServiceReply(senderUUID=CLIENT_ID,
                                       receiverUUID=request_sender,
                                       serviceType=request_service_type,
                                       results=results,
                                       msgUUID=reply_uuid,
                                       replyingToUUID=request_msg_id)
        msg = service_reply.msg.__str__()
        self._broker.send(receiver_endpoints=[request_reply_endpoint], msg=msg)
