PONSSE_MODEL = {
    "thingId": "s3i:5e83b933-331f-4278-b318-b7fdcb0e4872",
    "policyId": "s3i:5e83b933-331f-4278-b318-b7fdcb0e4872",
    "attributes": {
        "class": "ml40::Thing",
        "name": "Ponsse Harvester",
        "roles": [{"class": "fml40::Harvester"}],
        "features": [
            {"class": "fml40::AcceptsProximityAlert"},
            {"class": "fml40::AcceptsFellingJobs"},
            {"class": "fml40::Harvests"},
            {"class": "ml40::ManagesJobs"},
            {"class": "fml40::ProvidesProductionData"},
            {
                "class": "ml40::Location",
                "longitude": 6.2492276,
                "latitude": 50.808797
            },
            {
                "class": "ml40::Shared",
                "targets": ["s3i:..."]
            }
        ]
    }
}
CLIENT_ID = "s3i:5e83b933-331f-4278-b318-b7fdcb0e4872"
CLIENT_SECRET = "9d34802b-3a2f-4558-8c63-f1af09a68c2d"
REPO_TOPIC = "s3i/5e83b933-331f-4278-b318-b7fdcb0e4872/things/twin/commands/modify"
GRANT_TYPE = "client_credentials"
ENDPOINT = "s3ibs://s3i:5e83b933-331f-4278-b318-b7fdcb0e4872"

