from s3i import IdentityProvider, TokenType, Broker, GetValueReply
import ast
from benedict import benedict
import threading
import time
import websocket
import json
import os
import uuid
import serial
from modelling_language.managingActor import *
import copy


class bcolors:
    """colors for the console log"""

    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class BaseVariable(object):
    IDP_URL = "https://idp.s3i.vswf.dev/"
    IDP_REALM = "KWH"
    BROKER_HOST = "rabbitmq.s3i.vswf.dev"
    REPO_URL = "wss://ditto.s3i.vswf.dev/ws/2"


class BaseThing(ManagingActor):
    def __init__(
        self,
        client_id,
        client_secret,
        fml40_data_model: dict,
        grant_type: str,
        is_broker: bool,
        is_repo: bool,
        is_gps=False,
        username=None,
        password=None,
        endpoint=None,
    ):
        super(BaseThing, self).__init__()
        self.fml40_data_model = benedict(fml40_data_model, keypath_separator="/")
        self.__client_id = client_id
        self.__client_secret = client_secret
        self.__grant_type = grant_type
        self.__username = username
        self.__password = password
        self.__endpoint = endpoint
        self.__is_broker = is_broker
        self.__is_repo = is_repo
        self.__is_gps = is_gps
        self.__token = ""

        self._broker = None
        self._ws = None
        self.__ws_connected = False
        self.__observers = []

        if self.__is_gps:
            self._ser = serial.Serial("/dev/ttyS0", 9600)

        self._latitude = 0
        self._longitude = 0

    def attach(self, observer):
        if observer not in self.__observers:
            self.__observers.append(observer)

    def detach(self, observer):
        if observer in self._observers:
            self.__observers.remove(observer)

    def notify(self, path, modifier=None):
        for observer in self.__observers:
            if modifier != observer:
                observer.update(self, path)

    def run_forever(self):
        try:
            print(
                "[S³I]: Launch gSFL {0}{1}{2}".format(
                    bcolors.OKGREEN, self.__class__.__name__, bcolors.ENDC
                )
            )
            print("[S³I]: Press ctrl + c to stop running the gSFL")
            self.__connect_with_idp()
            threading.Thread(target=self.simulate_function).start()

        except KeyboardInterrupt:
            if self.__is_repo:
                self._ws.close()
            if self.__is_gps:
                self._ser.close()
            self.close_something_before_exit()
            time.sleep(1)
            print("exiting")
            os._exit(0)

    def close_something_before_exit(self):
        pass

    def __connect_with_idp(self):
        print(
            bcolors.OKBLUE
            + "[S³I][IdP]"
            + bcolors.ENDC
            + ": Connect with S3I IdentityProvider"
        )
        idp = IdentityProvider(
            grant_type=self.__grant_type,
            client_id=self.__client_id,
            username=self.__username,
            password=self.__password,
            client_secret=self.__client_secret,
            realm=BaseVariable.IDP_REALM,
            identity_provider_url=BaseVariable.IDP_URL,
        )
        idp.run_forever(token_type=TokenType.ACCESS_TOKEN, on_new_token=self.__on_token)

    def __on_token(self, token):
        print(bcolors.OKBLUE + "[S³I][IdP]" + bcolors.ENDC + ": get an access token")
        if self.__ws_connected:
            self._ws.close()

        self.__token = token
        if self.__is_broker:
            print(
                bcolors.OKBLUE
                + "[S³I][Broker]"
                + bcolors.ENDC
                + ": Connect with S3I Broker"
            )
            self.__connect_with_broker()
        if self.__is_repo:
            print(
                bcolors.OKBLUE
                + "[S³I][Repo]"
                + bcolors.ENDC
                + ": Connect with S3I Repository via websocket"
            )
            self.__connect_with_repo()

    def __connect_with_repo(self):
        self._ws = websocket.WebSocketApp(
            BaseVariable.REPO_URL,
            header={"Authorization: Bearer {}".format(self.__token)},
            on_message=self.__on_new_websocket_message,
            on_error=self.__on_new_websocket_error,
            on_open=self.__on_websocket_connection_opened,
            on_close=self.__on_websocket_connection_closed,
        )

        threading.Thread(target=self._ws.run_forever).start()

    @staticmethod
    def __on_new_websocket_message(ws, msg):
        pass

    @staticmethod
    def __on_new_websocket_error(ws, error):
        print(bcolors.OKBLUE + "[S³I][Repo]" + bcolors.ENDC + " : Websocekt error")

    def __on_websocket_connection_opened(self):
        self.__ws_connected = True
        self._ws.send("START-SEND-MESSAGES")

    def __on_websocket_connection_closed(self):
        self.__ws_connected = False

    def sync_with_repo(self, path, topic):

        if not self.__ws_connected:
            return None
        msg = {"topic": topic, "path": path, "value": self.fml40_data_model[path]}
        self._ws.send(json.dumps(msg))

    def __connect_with_broker(self):
        broker = Broker(
            auth_form="Username/Password",
            username=" ",
            password=self.__token,
            host=BaseVariable.BROKER_HOST,
        )
        self._broker = broker
        threading.Thread(
            target=self._broker.receive,
            args=(self.__endpoint, self.__on_broker_callback),
        ).start()

    def __on_broker_callback(self, ch, method, properties, body):
        body_json = ast.literal_eval(body.decode("utf-8"))
        if body_json.get("messageType") == "getValueRequest":
            self.on_get_value_request(body_json)
        elif body_json.get("messageType") == "serviceRequest":
            self.on_service_request(body_json)
        else:
            return None

    @staticmethod
    def nmea2latlong(nmea):
        pos1 = nmea.find(str.encode("$GPRMC"))
        pos2 = nmea.find(str.encode("\n"))
        loc = nmea[pos1:pos2]
        data = loc.split(str.encode(","))
        if len(data) > 5:
            lat2_dms = float(data[3])
            long2_dms = float(data[5])

            # convert to the real latitude and longitude
            latitude = lat2_dms * 0.337042402315857 + 51.45262998
            longitude = long2_dms * 0.526621755422343 + 7.99501874
        else:
            latitude = 0
            longitude = 0
        return [latitude, longitude]

    def read_position(self):
        if self.__is_gps:
            try:
                if self._ser.inWaiting() > 100:
                    gps = self._ser.readline()
                    [self._latitude, self._longitude] = self.nmea2latlong(gps)
                else:
                    return None
            except serial.SerialException:
                return None

    def on_get_value_request(self, msg):
        get_value_reply = GetValueReply()
        request_sender = msg.get("sender")
        request_msg_id = msg.get("identifier")
        request_sender_endpoint = msg.get("replyToEndpoint")
        attribute_path = msg.get("attributePath")
        reply_msg_uuid = "s3i:" + str(uuid.uuid4())
        try:
            value = {attribute_path: self._uriToData(attribute_path)}
        except KeyError:
            value = {attribute_path: "invalid attribute path"}

        get_value_reply.fillGetValueReply(
            senderUUID=self.__client_id,
            receiverUUID=[request_sender],
            results=value,
            msgUUID=reply_msg_uuid,
            replyingToUUID=request_msg_id,
        )
        self._broker.send(
            receiver_endpoints=[request_sender_endpoint],
            msg=get_value_reply.msg.__str__(),
        )

    resGetValue = list()

    def _uriToData(self, uri):
        if uri == "":
            return self.fml40_data_model
        else:
            uri_list = uri.split("/")
            print(uri_list)
            try:
                self._getValue(self.fml40_data_model, uri_list)
            except:
                return "invalid attribute path"
            print(resGetValue)
            if resGetValue.__len__() == 0:
                return "invalid attribute path"
            response = copy.deepcopy(resGetValue)
            resGetValue.clear()
            if response.__len__() == 1:
                return response[0]
            else:
                return response

    def _getValue(self, source, uri_list):
        value = source[uri_list[0]]
        if uri_list.__len__() == 1:
            # if is ditto-feature
            if isinstance(value, str):
                try:
                    stringValue_split = value.split(":")
                    if stringValue_split[0] == "ditto-feature":
                        value = self.fml40_data_model["features"][stringValue_split[1]][
                            "properties"
                        ][uri_list[0]]
                except:
                    pass
            # print("Der gesuchte Wert: ", value)
            resGetValue.append(value)
            return
        if isinstance(value, dict):
            del uri_list[0]
            self._getValue(value, uri_list)
        if isinstance(value, list):
            if isinstance(value[0], (str, int, float, bool, list)):
                return value
            if isinstance(value[0], dict):
                for item in value:
                    if item["class"] == "ml40::Thing":
                        for i in item["roles"]:
                            if self._findValue(i, uri_list[1]):
                                uri_list_1 = copy.deepcopy(uri_list)
                                del uri_list_1[:2]
                                self._getValue(item, uri_list_1)
                    else:
                        if self._findValue(item, uri_list[1]):
                            uri_list_1 = copy.deepcopy(uri_list)
                            del uri_list_1[:2]
                            if not uri_list_1:
                                # print("Der gesuchte Wert: ", item)
                                resGetValue.append(item)
                                return
                            else:
                                self._getValue(item, uri_list_1)
        if isinstance(value, (str, int, float, bool)):
            # if is ditto-feature
            if isinstance(value, str):
                try:
                    stringValue_split = value.split(":")
                    if stringValue_split[0] == "ditto-feature":
                        value = self.fml40_data_model["features"][stringValue_split[1]][
                            "properties"
                        ][uri_list[0]]
                except:
                    pass
            # print("Der gesuchte Wert: ", value)
            resGetValue.append(value)

    def _findValue(self, json, value):
        for item in json:
            if json[item] == value:
                # print("Parameter: ", json[item])
                return True
        return False

    def on_service_request(self, body_json):
        pass

    def simulate_function(self, run=False):
        pass
