
proximity_alert_date_model = {
    "thingId": "s3i:c8d7d60d-d682-4798-88a1-0cfc3aec5bc0",
    "policyId": "s3i:c8d7d60d-d682-4798-88a1-0cfc3aec5bc0",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "ml40::Service"}],
        "name": "proximity alert", 
        "features": [
            {
                "class": "fml40::MonitorsProximity"
            }
        ]
    },
    "features": {}

}