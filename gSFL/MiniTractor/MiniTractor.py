from ..BaseThing import BaseThing
from .Config import *
from modelling_language.ml40 import ProvidesOperationalData
from s3i.messages import *
import uuid
from .FunctionImplementation import MoveImplementation, ShieldImplementation, WinchImplementation, MiniTractorController
import threading
import time


class MiniTractor(BaseThing):
    def __init__(self, bluetooth_controller):
        super(MiniTractor, self).__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            grant_type=GRANT_TYPE,
            endpoint=ENDPOINT,
            fml40_data_model=MINI_TRACTOR_DATE_MODEL,
            is_broker=True,
            is_repo=True,
            is_gps=True
        )
        self.controller = bluetooth_controller
        ProvidesOperationalData.start(ref_managing_actor=self.actor_ref, name="ml40::ProvidesOperationalData").proxy()
        MoveImplementation.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsMoveCommands",
                                 controller=self.controller).proxy()
        ShieldImplementation.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsShieldCommands",
                                   controller=self.controller).proxy()
        WinchImplementation.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsWinchCommands",
                                  controller=self.controller).proxy()

    def on_service_request(self, msg):
        service_reply = ServiceReply()
        request_sender = msg.get("sender")
        request_msg_id = msg.get("identifier")
        request_reply_endpoint = msg.get("replyToEndpoint")
        request_service_type = msg.get("serviceType")
        parameters = msg.get("parameters")
        reply_uuid = "s3i:{}".format(str(uuid.uuid4()))

        if request_service_type not in self.registeredServices:
            results = {"availableServiceType": self.registeredServices}

        elif request_service_type == "fml40::AcceptsMoveCommands":
            direction = parameters.get("direction")
            speed_level = parameters.get("speed_level")
            self.proxyFunctionalities["fml40::AcceptsMoveCommands"].move(direction, speed_level)
            results = {"Move": direction}

        elif request_service_type == "fml40::AcceptsWinchCommands":
            operation = parameters.get("operation")
            speed_level = parameters.get("speed_level")
            if operation == "rollUp":
                self.proxyFunctionalities["fml40::AcceptsWinchCommands"].rollUp(speed_level)
            elif operation == "rollDown":
                self.proxyFunctionalities["fml40::AcceptsWinchCommands"].rollDown(speed_level)
            results = {"Winch": operation}

        elif request_service_type == "fml40::AcceptsShieldCommands":
            operation = parameters.get("operation")
            speed_level = parameters.get("speed_level")
            if operation == "shieldUp":
                self.proxyFunctionalities["fml40::AcceptsShieldCommands"].shieldUp(speed_level)
            elif operation == "shieldDown":
                self.proxyFunctionalities["fml40::AcceptsShieldCommands"].shieldDown(speed_level)
            results = {"Shield": operation}

        elif request_service_type == "ml40::ProvidesOperationalData":
            report = self.proxyFunctionalities["ml40::ProvidesOperationalData"].getOperationalData()
            results = {"report": report.get()}

        service_reply.fillServiceReply(senderUUID=CLIENT_ID,
                                       receiverUUID=request_sender,
                                       serviceType=request_service_type,
                                       results=results,
                                       msgUUID=reply_uuid,
                                       replyingToUUID=request_msg_id)

        msg = service_reply.msg.__str__()
        self._broker.send(receiver_endpoints=[request_reply_endpoint], msg=msg)

    def close_something_before_exit(self):
        self.controller.close_bluetooth_connection = True
        
    def simulate_function(self, run=True):
        while run:
            self.read_position()
            #print(self._latitude)
            #print(self._longitude)
            if self.fml40_data_model[LATITUDE_PATH] != self._latitude:
                self.fml40_data_model[LATITUDE_PATH] = self._latitude
                self.sync_with_repo(LATITUDE_PATH, REPO_TOPIC)

            if self.fml40_data_model[LONGITUDE_PATH] != self._longitude:
                self.fml40_data_model[LONGITUDE_PATH] = self._longitude
                self.sync_with_repo(LONGITUDE_PATH, REPO_TOPIC)

            if self.fml40_data_model[MOTOR_LEFT_PATH] != self.controller.left_motor_ist_speed:
                self.fml40_data_model[MOTOR_LEFT_PATH] = self.controller.left_motor_ist_speed
                self.sync_with_repo(MOTOR_LEFT_PATH, REPO_TOPIC)

            if self.fml40_data_model[MOTOR_RIGHT_PATH] != self.controller.right_motor_ist_speed:
                self.fml40_data_model[MOTOR_RIGHT_PATH] = self.controller.right_motor_ist_speed
                self.sync_with_repo(MOTOR_RIGHT_PATH, REPO_TOPIC)

            if self.fml40_data_model[WINCH_LENGTH_PATH] != self.controller.winch_length:
                self.fml40_data_model[WINCH_LENGTH_PATH] = self.controller.winch_length
                self.sync_with_repo(WINCH_LENGTH_PATH, REPO_TOPIC)

            if self.fml40_data_model[SHIELD_LIFT_PATH] != self.controller.shield_lift:
                self.fml40_data_model[SHIELD_LIFT_PATH] = self.controller.shield_lift
                self.sync_with_repo(SHIELD_LIFT_PATH, REPO_TOPIC)
            time.sleep(0.1)
