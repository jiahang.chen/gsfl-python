MINI_TRACTOR_DATE_MODEL = {
    "thingId": "s3i:847dc67e-9dad-4415-8e58-819b724a1a8f",
    "policyId": "s3i:847dc67e-9dad-4415-8e58-819b724a1a8f",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "fml40::MiniTractor"}],
        "features": [
            {
                "class": "ml40::OperatingHours",
                "total": 121.1
            },
            {
                "class": "ml40::LastServiceCheck",
                "timestamp": "2020-03-20"
            },
            {
                "class": "ml40::Weight",
                "weight": 1400
            },
            {
                "class": "ml40::Dimensions",
                "length": 2.2,
                "width": 1.5,
                "height": 1.205
            },
            {
                "class": "ml40::OrientationRPY",
                "roll": "ditto-feature:id1",
                "pitch": "ditto-feature:id1",
                "yaw": "ditto-feature:id1"
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature: id2",
                "latitude": "ditto-feature: id2",
                "orientation": "ditto-feature: id2"
            },
            {
                "class": "fml40::AcceptsMoveCommands"
            },
            {
                "class": "fml40::AcceptsWinchCommands"
            },
            {
                "class": "fml40::AcceptsShieldCommands"
            },
            {
                "class": "fml40::AcceptsFellingSupportsJobs"
            },
            {
                "class": "fml40::AcceptsForwardingJobs"
            },
            {
                "class": "ml40::JobList",
                "subFeatures": [
                    {
                        "class": "fml40::ForwardingJob",
                        "status": "Complete"
                    },
                    {
                        "class": "fml40::FellingSupportJob",
                        "status": "InProgress"
                    },
                    {
                        "class": "fml40::FellingSupportJob",
                        "status": "Pending" 
                    }
                ]
            },
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Winch",
                        "roles": [{"class": "fml40::Winch"}],
                        "features": [
                            {
                                "class": "ml40::ExpansionLength",
                                "maxLength": "ditto-feature:id3",
                                "currentLength": "ditto-feature:id3"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Engine left",
                        "roles": [{"class": "ml40::Engine"}],
                        "features": [
                            {
                                "class": "ml40::RotationalSpeed",
                                "rpm": "ditto-feature:id4"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Engine right",
                        "roles": [{"class": "ml40::Engine"}],
                        "features": [
                            {
                                "class": "ml40::RotationalSpeed",
                                "rpm": "ditto-feature:id5"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Rückeschild",
                        "roles": [{"class": "fml40::StackingShield"}],
                        "features": [
                            {
                               "class": "ml40::Lift",
                               "minLift": "ditto-feature:id6",
                               "currentLift": "ditto-feature:id6",
                               "maxLift": "ditto-feature:id6"
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "roll": 0.0,
                "pitch": 1.0,
                "yaw": -0.5
            }
        },
        "id2": {
            "properties": {
                "longitude": 6.041209,
                "latitude": 50.787340,
                "orientation": 90
            }
        },
        "id3": {
            "properties": {
                "maxLength": 50,
                "currentLength": 3
            }
        },
        "id4": {
            "properties": {
               "rpm": 30
            }
        },
        "id5": {
            "properties": {
                "rpm": -40
            }
        },
        "id6": {
            "properties": {
               "minLift": 0,
               "currentLift": 0.0,
               "maxLift": 3.7
            }
        }
    }
}

GRANT_TYPE = "client_credentials"
CLIENT_ID = "s3i:847dc67e-9dad-4415-8e58-819b724a1a8f"
CLIENT_SECRET = "c530e2c1-97cb-44b0-a98d-43720ccfe199"
ENDPOINT = "s3ibs://s3i:847dc67e-9dad-4415-8e58-819b724a1a8f"
LONGITUDE_PATH = "features/id2/properties/longitude"
LATITUDE_PATH = "features/id2/properties/latitude"
ORIENTATION_PATH = "features/id2/properties/orientation"
MOTOR_LEFT_PATH = "features/id4/properties/rpm"
MOTOR_RIGHT_PATH = "features/id5/properties/rpm"
WINCH_LENGTH_PATH = "features/id3/properties/currentLength"
SHIELD_LIFT_PATH = "features/id6/properties/currentLift" 
REPO_TOPIC = "s3i/847dc67e-9dad-4415-8e58-819b724a1a8f/things/twin/commands/modify"
