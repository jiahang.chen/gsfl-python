from modelling_language.fml40 import AcceptsMoveCommands, AcceptsShieldCommands, AcceptsWinchCommands
from curio import sleep
from bricknil import attach, start
from bricknil.hub import CPlusHub
from bricknil.sensor.motor import CPlusLargeMotor, CPlusXLMotor
from bricknil.sensor.light import LED
from bricknil.const import Color
from enum import Enum


class MoveImplementation(AcceptsMoveCommands):
    def __init__(self, name, ref_managing_actor, controller):
        self.controller = controller
        super(MoveImplementation, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor)
        
    def move(self, direction, speed): 
        print("i am moving in direction {} with speed {}".format(direction, speed))
        if not isinstance(direction, float):
            direction = float(direction)
        if not isinstance(speed, float):
            speed = float(speed)
        self.controller.moving_control(direction=direction, speed_level=speed)


class WinchImplementation(AcceptsWinchCommands):
    def __init__(self, name, ref_managing_actor, controller):
        self.controller = controller
        super(WinchImplementation, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor)

    def rollUp(self, speed):
        print("i am rolling the cable winch up with speed {}".format(speed))
        if not isinstance(speed, float):
            speed = float(speed)
        self.controller.winch_control(operation="rollUp", speed_level=speed)

    def rollDown(self, speed):
        print("i am rolling the cable winch down with speed {}".format(speed))
        if not isinstance(speed, float):
            speed = float(speed)
        self.controller.winch_control(operation="rollDown", speed_level=speed)


class ShieldImplementation(AcceptsShieldCommands):
    def __init__(self, name, ref_managing_actor, controller):
        self.controller = controller
        super(ShieldImplementation, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor
        )

    def shieldUp(self, speed):
        print("i am raising the shield up with speed {}".format(speed))
        if not isinstance(speed, float):
            speed = float(speed)
        self.controller.shield_control(operation="shieldUp", speed_level=speed)
    
    def shieldDown(self, speed):
        print("i am sinking the shield down with speed {}".format(speed))
        if not isinstance(speed, float):
            speed = float(speed)
        self.controller.shield_control(operation="shieldDown", speed_level=speed)


class SpeedLevel(Enum):
    MOTOR_SPEED_LIMIT = 45
    SHIELD_SPEED_LIMIT = 50
    WINCH_SPEED_LIMIT = 50


@attach(LED, name="led")
@attach(CPlusXLMotor, name="right_motor",capabilities=['sense_speed'], port=0)
@attach(CPlusXLMotor, name="left_motor", capabilities=['sense_speed'], port=1)
@attach(CPlusLargeMotor, name="winch", capabilities=['sense_speed',('sense_pos',10)], port=2)
@attach(CPlusLargeMotor, name="shield", capabilities=['sense_speed',('sense_pos',5)], port=3)
class MiniTractorController(CPlusHub):
    def __init__(self, name, run=False):
        super().__init__(name, run)
        self.__port_0_speed = 0
        self.__port_1_speed = 0
        self.__port_2_speed = 0
        self.__port_3_speed = 0

        self.__step_time = 1  # 1s
        self.__right_motor_ist_speed = 0
        self.__left_motor_ist_speed = 0
        
        self.__winch_ist_speed = 0
        self.__winch_ist_pos = 0
        self.__winch_length = 0
        
        self.__shield_ist_speed = 0
        self.__shield_ist_pos = 0
        self.__shield_lift = 0 
        
        self.__is_winch_init = False
        self.__is_shield_init = False

        self.__winch_initial_state_pos = 0
        self.__shield_initial_state_pos = 0

        self.keep_running = False
        self.close_bluetooth_connection = False 
    @property
    def right_motor_ist_speed(self):
        return self.__right_motor_ist_speed

    @property
    def left_motor_ist_speed(self):
        return self.__left_motor_ist_speed

    @property
    def winch_ist_speed(self):
        return self.__winch_ist_speed

    @property
    def shield_ist_speed(self):
        return self.__shield_ist_speed

    @property
    def winch_length(self):
        return self.__winch_length

    @property
    def shield_lift(self):
        return self.__shield_lift
        
    async def right_motor_change(self):
        speed = self.right_motor.value[CPlusXLMotor.capability.sense_speed]
        if 0 <= speed <= 127:
            self.__right_motor_ist_speed = speed
        elif 127 < speed <= 255:
            self.__right_motor_ist_speed = speed - 255
            
        #print("right motor speed: {}".format(self.__right_motor_ist_speed))
         
    async def left_motor_change(self):
        speed = self.left_motor.value[CPlusXLMotor.capability.sense_speed]
        if 0 <= speed <= 127:
            self.__left_motor_ist_speed = speed
        elif 127 <= speed <= 255:
            self.__left_motor_ist_speed = speed - 255
            
        #print("left motor speed {}".format(self.__left_motor_ist_speed))

    async def winch_change(self):
        speed = self.winch.value[CPlusLargeMotor.capability.sense_speed]
        pos = self.winch.value[CPlusLargeMotor.capability.sense_pos]
        
        if 0 <= speed < 127:
            self.__winch_ist_speed = speed
        elif 127 <= speed <= 255:
            self.__winch_ist_speed = speed - 255

        if self.__is_winch_init:
            if 4294967298 > pos >= (self.__winch_initial_state_pos-1000):
                self.__winch_length = (pos - self.__winch_initial_state_pos)/1800 * 16
            else:
                self.__winch_length = (4294967298 - self.__winch_initial_state_pos + pos)/1800 * 16

            if self.__winch_length < 0:
                self.__winch_length = 0
            elif self.__winch_length > 50:
                self.__winch_length = 50
                
        print("seilwinde length: {}".format(self.__winch_length))
        
    async def shield_change(self):
        speed = self.shield.value[CPlusLargeMotor.capability.sense_speed]
        pos = self.shield.value[CPlusLargeMotor.capability.sense_pos]
        
        if 0 <= speed < 127:
            self.__shield_ist_speed = speed
        elif 127 <= speed <= 255:
            self.__shield_ist_speed = speed - 255

        if self.__is_shield_init:
            if 4294967298 > pos > (self.__shield_initial_state_pos - 1000):
                self.__shield_lift = 3.7 - (pos - self.__shield_initial_state_pos)/3800 * 3.7
            else:
                self.__shield_lift = 3.7 - (4294967298 - self.__shield_initial_state_pos + pos)/3800 * 3.7

            """
            Messfehler 
            """
            
            if self.__shield_lift < 0:
                self.__shield_lift = 0
            elif self.__shield_lift > 3.7:
                self.__shield_lift = 3.7
            
        print("shield lift: {}".format(self.__shield_lift))
        
    async def initial(self):
        await sleep(30)
        while True:            
            if self.__winch_ist_speed == 0:
                self.__is_winch_init = True
                self.__winch_length = 0
            
            if self.__shield_ist_speed == 0:
                self.__is_shield_init = True
                self.__shield_lift = 3.7
            print(self.__is_winch_init)
            print(self.__is_shield_init)
            if self.__is_winch_init and self.__is_shield_init:
                self.keep_running = True
                self.__winch_initial_state_pos = self.winch.value[CPlusLargeMotor.capability.sense_pos]
                self.__shield_initial_state_pos = self.shield.value[CPlusLargeMotor.capability.sense_pos]
                await self.led.set_color(Color.blue)
                break
            await sleep(1)
            
    async def run(self):
        self.message_info("[gSFL]: Demo Running")
        self.message_info("[gSFL]: Reset the demo to initial state")
        await self.led.set_color(Color.green)        
        await self.winch.set_speed(-15)
        await self.shield.set_speed(-15)
    
        await self.initial()
            
        await self.winch.set_speed(0)
        await self.shield.set_speed(0)
        await sleep(self.__step_time) 
        
        while self.keep_running:
            print("test {}".format(self.keep_running))
            if self.__port_0_speed != 0:
                print("port 0: {}".format(self.__port_0_speed))
                self.__port_2_speed = 0
                self.__port_3_speed = 0
            if self.__port_1_speed != 0:
                print("port 1: {}".format(self.__port_1_speed))
                self.__port_2_speed = 0
                self.__port_3_speed = 0
            if self.__port_2_speed != 0:
                print("port 2: {}".format(self.__port_2_speed))
                self.__port_0_speed = 0
                self.__port_1_speed = 0
            if self.__port_3_speed != 0:
                print("port 3: {}".format(self.__port_3_speed))
                self.__port_0_speed = 0
                self.__port_1_speed = 0
                
            await self.left_motor.set_speed(int(self.__port_1_speed))
            await self.right_motor.set_speed(int(self.__port_0_speed))
            await self.winch.set_speed(int(self.__port_2_speed))
            await self.shield.set_speed(int(self.__port_3_speed))
            await sleep(self.__step_time)
            
            if self.__winch_length <= 0 and self.__port_2_speed < 0:
                self.__port_2_speed = 0

            elif self.__winch_length >= 50 and self.__port_2_speed > 0:
                self.__port_2_speed = 0

            if self.__shield_lift <= 0 and self.__port_3_speed > 0:
                self.__port_3_speed = 0
                
            elif self.__shield_lift >= 3.7 and self.__port_3_speed < 0 :
                self.__port_3_speed = 0
                
            if self.close_bluetooth_connection:
                break
            
    def moving_control(self, direction, speed_level):
        
        if 90 >= direction >= 0:
            self.__port_1_speed = - (SpeedLevel.MOTOR_SPEED_LIMIT.value + direction/2) * speed_level
            self.__port_0_speed = speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value - direction/2)

        elif 180 >= direction > 90:
            self.__port_1_speed = speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value + (180 - direction)/2)
            self.__port_0_speed = -speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value - (180 - direction)/2)

        elif 270 > direction > 180:
            self.__port_1_speed = speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value - (direction - 180)/2)
            self.__port_0_speed = -speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value + (direction - 180)/2)

        elif 360 >= direction > 270:
            self.__port_1_speed = -speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value - (360-direction)/2)
            self.__port_0_speed = speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value  + (360-direction)/2)
        

    def winch_control(self, operation, speed_level):
        if self.__is_winch_init and self.__is_shield_init:
            if operation == "rollUp" and self.winch_length > 0:
                self.__port_2_speed = - SpeedLevel.WINCH_SPEED_LIMIT.value * speed_level

            elif operation == "rollDown" and self.winch_length < 50:
                self.__port_2_speed = SpeedLevel.WINCH_SPEED_LIMIT.value * speed_level

    def shield_control(self, operation, speed_level):
        if self.__is_winch_init and self.__is_shield_init:

            if operation == "shieldUp" and self.shield_lift < 3.7:
                self.__port_3_speed = - SpeedLevel.SHIELD_SPEED_LIMIT.value * speed_level

            elif operation == "shieldDown" and self.shield_lift > 0:
                self.__port_3_speed = SpeedLevel.SHIELD_SPEED_LIMIT.value * speed_level



    
