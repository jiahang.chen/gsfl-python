KOMATSU_MODEL= {
    "thingId": "s3i:3154edfa-5b04-4a28-b803-d6ec46135c19",
    "policyId": "s3i:3154edfa-5b04-4a28-b803-d6ec46135c19",
    "attributes": {
        "class": "ml40::Thing",
        "name": "Komatsu Forwarder",
        "roles": [{"class": "fml40::Forwarder"}],
        "features": [
            {"class": "fml40::AcceptsProximityAlert"},
            {"class": "fml40::AcceptsForwardingJobs"},
            {"class": "fml40::Forwards"},
            {
                "class": "ml40::Location",
                "longitude": 6.2492276,
                "latitude": 50.808797
            },
            {
                "class": "ml40::Shared",
                "targets": ["s3i:..."]
            },
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "roles": ["ml40::Engine"],
                        "name": "Motor",
                        "identifier": "s3i:...",
                        "features": [{
                            "class": "ml40::RotationalSpeed",
                            "rpm": "3000"
                        }]
                    },
                    {
                        "class": "ml40::Thing",
                        "roles": ["ml40::Crane"],
                        "name": "Kran",
                        "identifier": "s3i:...",
                        "features": [{
                            "class": "ml40::Composite",
                            "targets": [{
                                "class": "ml40::Thing",
                                "roles": ["fml40::ForwarderCrane"],
                                "name": "Forwarder Kran",
                                "identifier": "s3i:...",
                                "features": [{"class": "fml40::Grabs"}]
                            }]
                        }]
                    },
                    {
                        "class": "ml40::Thing",
                        "roles": ["ml40::MachineUI"],
                        "name": "Bordcomputer",
                        "identifier": "s3i:..."
                    }
                ]
            }
        ]
    }
}

CLIENT_ID = KOMATSU_MODEL.get("thingId")
CLIENT_SECRET = "432f0bfc-9a13-4551-a4cd-b94b054cdadf"
ENDPOINT = "s3ibs://{}".format(CLIENT_ID)
GRANT_TYPE = "client_credentials"
REPO_TOPIC = "s3i/3154edfa-5b04-4a28-b803-d6ec46135c19/things/twin/commands/modify"
