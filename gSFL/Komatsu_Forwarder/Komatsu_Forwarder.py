from .Config import *
from modelling_language.fml40 import AcceptsProximityAlert, AcceptsForwardingJobs, Forwards
from ..BaseThing import BaseThing


class KomatsuForwarder(BaseThing):
    def __init__(self):
        super().__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            endpoint=ENDPOINT,
            fml40_data_model=KOMATSU_MODEL,
            grant_type=GRANT_TYPE,
            is_broker=True,
            is_repo=False
        )
        AcceptsProximityAlert.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsProximityAlert").proxy()
        AcceptsForwardingJobs.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsForwardingJobs").proxy()
        Forwards.start(ref_managing_actor=self.actor_ref, name="fml40::Forwards").proxy()

    def on_service_request(self, body_json):
        service_type = body_json.get("serviceType")
        if service_type == "fml40::ForwardingJob":
            job = body_json["parameters"].get("job")
            accepted = self.proxyFunctionalities["fml40::AcceptsForwardingJobs"].acceptJob(job)
            if accepted.get():
                self.proxyFunctionalities["fml40::Forwards"].executeJob(job)

