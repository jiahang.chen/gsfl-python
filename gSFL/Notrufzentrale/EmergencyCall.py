emergency_center_date_model = {
    "thingId": "s3i:be074842-7d3c-4956-bc9d-c322808c4b65",
    "policyId": "s3i:be074842-7d3c-4956-bc9d-c322808c4b65",
    "attributes": {
        "class": "ml40::Thing",
        "name": "DT of emergency center",
        "features": [
            {
                "class": "fml40::MonitorsHealthStatus"
            },
            {
                "class": "ml40::Moisture"
            },
            {
                "class": "fml40::DisplaysHealthAlarms"
            }
        ]
    },
    "features": {

    }
}