FORWARDER_B_MODEL = {
    "thingId": "s3i:f3302964-0110-424f-81b7-dc90127cd745",
    "policyId": "s3i:f3302964-0110-424f-81b7-dc90127cd745",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "fml40::Forwarder"}],
        "name": "DT of Forwarder B",
        "features": [
            {
                "class": "fml40::Forwards",
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature: id1",
                "latitude": "ditto-feature: id2"
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "longitude": ""
            }
        },
        "id2": {
            "properties": {
                "latitude": ""
            }
        }
    }
}

GRANT_TYPE = "client_credentials"
CLIENT_ID = "s3i:f3302964-0110-424f-81b7-dc90127cd745"
CLIENT_SECRET = "fa9af2d7-4c76-4512-b850-6601aaa0fcee"
ENDPOINT = "s3ib://s3i:f3302964-0110-424f-81b7-dc90127cd745"
LONGITUDE_PATH = "features/id1/properties/longitude"
LATITUDE_PATH = "features/id2/properties/latitude"
REPO_TOPIC = "s3i/f3302964-0110-424f-81b7-dc90127cd745/things/twin/commands/modify"
