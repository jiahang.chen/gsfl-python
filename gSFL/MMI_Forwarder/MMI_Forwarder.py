from ..BaseThing import BaseThing
from .Config import *
from s3i.messages import *


class MMIForwarder(BaseThing):
    def __init__(self):
        super(MMIForwarder, self).__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            grant_type=GRANT_TYPE,
            endpoint=ENDPOINT,
            fml40_data_model=MMI_FORWARDER_MODEL,
            is_broker=True,
            is_repo=True,
            is_gps=False
        )
