MMI_FORWARDER_MODEL = {
    "thingId": "s3i:08bf8c98-ad4e-4bc1-b823-7667e73688ac",
    "policyId": "s3i:08bf8c98-ad4e-4bc1-b823-7667e73688ac",
    "attributes": {
        "class": "ml40::Thing",
        "name": "MMI Forwarder",
        "roles": [{"class": "fml40::Forwarder"}],
        "features": [
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Waage am Forwarder",
                        "roles": [{"class": "fml40::Scale"}],
                        "features": [
                            {
                                "class": "ml40::Weight",
                                "currentWeight": "ditto-feature:id1"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "features": [
                            {
                                "class": "ml40::Weight",
                                "Weight": 1000
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Tank des Forwarders",
                        "roles": [{"class": "fml40::Tank"}],
                        "features": [
                            {
                                "class": "ml40::LiquidFillingLevel",
                                "maxLevel": "ditto-feature:id2",
                                "currentLevel": "ditto-feature:id2"
                            }
                        ]
                    }
                ]
            },
            {
                "class": "ml40::Dimensions",
                "weight": 15000
            },
            {
                "class": "fml40::Forwards"
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature:id3",
                "latitude": "ditto-feature:id3"
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "currentWeight": 300
            }
        },
        "id2": {
            "properties": {
                "maxLevel": 300,
                "currentLevel": 100
            }
        },
        "id3": {
            "properties": {
                "longitude": 52.42432,
                "latitude": 10.534
            }
        }
    }
}

GRANT_TYPE = "client_credentials"
CLIENT_ID = "s3i:08bf8c98-ad4e-4bc1-b823-7667e73688ac"
CLIENT_SECRET = "359d9940-6c75-4330-8141-6307ec283d31"
ENDPOINT = "s3ib://" + CLIENT_ID
CUR_WEIGHT_PATH = "features/id1/properties/currentWeight"
CUR_TANK_LEVEL_PATH = "features/id2/properties/currentLevel"
MAX_TANK_LEVEL_PATH = "features/id2/properties/maxLevel"
LONGITUDE_PATH = "features/id3/properties/longitude"
LATITUDE_PATH = "features/id3/properties/latitude"
REPO_TOPIC = "s3i/08bf8c98-ad4e-4bc1-b823-7667e73688ac/things/twin/commands/modify"
