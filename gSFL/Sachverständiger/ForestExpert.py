from .BaseThing import *

forest_expert_date_model = {
    "thingId": "s3i:2a2727eb-3be6-4c53-9300-6269a0969d43",
    "policyId": "s3i:2a2727eb-3be6-4c53-9300-6269a0969d43",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "fml40::ForestOwner"}],
        "name": "DT of Forest Expert",
        "features": [
            {
                "class": "ml40::PersonalName",
                "firstName": "TODO",
                "lastName": "Schmitz"
            },
            {
                "class": "ml40::Location",
                "latitude": "ditto-feature:id1",
                "longitude": "ditto-feature:id2"
            },
            {
                "class": "ml40::Address",
                "streetnumber": "TODO",
                "city": "TODO",
                "zip": "TODO",
                "street": "TODO",
                "country": "TODO"
            }
        ]
    },
    "features": {
        "io1": {
            "properties": {
                "latitude": 8.052771
            }
        },
        "id2": {
            "properties": {
                "longitude": 51.407337
            }
        }
    }
}

