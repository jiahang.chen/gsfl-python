
pass_ability_service_date_model = {
    "thingId": "s3i:1367de5a-6b9d-4403-90a7-7ee9eb157b43",
    "policyId": "s3i:1367de5a-6b9d-4403-90a7-7ee9eb157b43",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "ml40::Service"}],
        "name": "pass ability service",
        "features": [
            {
                "class": "fml40::AcceptsMoistureMeasurement"
            },
            {
                "class": "fml40::ProvidesPassabilityInformation"
            }
        ]
    },
    "features": {

    }

}


