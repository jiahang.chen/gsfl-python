from ..BaseThing import BaseThing
from s3i.messages import *
import uuid
from .Config import *


class ForestUnit(BaseThing):
    def __init__(self):
        super().__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            endpoint=ENDPOINT,
            grant_type=GRANT_TYPE,
            is_broker=True,
            is_repo=False
        )

    @staticmethod
    def send_service_request(self):
        sender = CLIENT_ID,
        receivers = ["s3i:6c77d7a9-1add-4751-8930-f4733ca5a2f4"]
        receiver_endpoints = ["s3ib://s3i:6c77d7a9-1add-4751-8930-f4733ca5a2f4"]
        serviceType = "fml40::Forwarding"
        msg_id = "s3i:{}".format(str(uuid.uuid4()))
        parameters = {"object": "stem segments"}
        req = ServiceRequest()
        req.fillServiceRequest(
            SenderUUID=sender,
            receiverUUID=receivers,
            sender_endpoint=ENDPOINT,
            serviceType=serviceType,
            parameters=parameters,
            msgUUID=msg_id
        )


