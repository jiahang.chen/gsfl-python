WZL_FORWARDER_MODEL = {
    "thingId": "s3i:6c77d7a9-1add-4751-8930-f4733ca5a2f4",
    "policyId": "s3i:6c77d7a9-1add-4751-8930-f4733ca5a2f4",
    "attributes": {
        "class": "ml40::Thing",
        "name": "WZL-Demo-Forwarder",
        "roles": [{"class": "ml40::Forwarder"}],
        "components": [
            {
                "class": "ml40::Scale",
                "currentWeight": 300
            },
            {
                "class": "fml40::LogLoadingArea",
                "maWeight": 10000
            },
            {
                "class": "ml40::Dimensions",
                "weight": 15000
            },
            {"class": "fml40::Forwarding"}
        ]
    }
}

CLIENT_ID = "s3i:6c77d7a9-1add-4751-8930-f4733ca5a2f4"
CLIENT_SECRET = "9a353553-c637-4b9b-9e6d-1661fdf98e4f"
ENDPOINT = "s3ib://s3i:a6884008-d55a-4fd0-896d-2220490bcc25"
GRANT_TYPE = "client_credentials"

