from ..BaseThing import BaseThing
from s3i.messages import *
import uuid
from .Config import *
from modelling_language.fml40 import AcceptsSingleTreeFellingJobs, AcceptsMoveCommands
import time


class ForestWorkerKarl(BaseThing):
    def __init__(self):
        username = USERNAME
        password = PASSWORD
        super(ForestWorkerKarl, self).__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            endpoint=ENDPOINT,
            fml40_data_model=KARL_MODEL,
            grant_type=GRANT_TYPE,
            username=username,
            password=password,
            is_broker=True,
            is_repo=True,
            is_gps=True
        )
        AcceptsSingleTreeFellingJobs.start(
            ref_managing_actor=self.actor_ref,
            name="fml40::AcceptsSingleTreeFellingJobs").proxy()
        AcceptsMoveCommands.start(
            ref_managing_actor=self.actor_ref, name="fml40::AcceptsMoveCommands").proxy()

    def on_service_request(self, msg):
        service_reply = ServiceReply()
        request_sender = msg.get("sender")
        request_msg_id = msg.get("identifier")
        request_reply_endpoint = msg.get("replyToEndpoint")
        request_service_type = msg.get("serviceType")
        parameters = msg.get("parameters")
        reply_uuid = "s3i:{}".format(str(uuid.uuid4()))
        if request_service_type not in self.registeredServices:
            results = {"availableServiceType": self.registeredServices}

        elif request_service_type == "fml40::AcceptsSingleTreeFellingJobs":
            job = parameters.get("job")
            accepted = self.proxyFunctionalities[request_service_type].acceptJob(job)
            results = {"accepted": accepted.get()}

        elif request_service_type == "fml40::AcceptsMoveCommands":
            direction = parameters.get("direction")
            speed = parameters.get("speed")
            self.proxyFunctionalities[request_service_type].move(direction, speed)
            results = {"moved": True}

        service_reply.fillServiceReply(senderUUID=CLIENT_ID,
                                       receiverUUID=request_sender,
                                       serviceType=request_service_type,
                                       results=results,
                                       msgUUID=reply_uuid,
                                       replyingToUUID=request_msg_id
                                       )

        self._broker.send(receiver_endpoints=[request_reply_endpoint], msg=service_reply.msg.__str__())

    def simulate_function(self, run=True):
        while run:
            self.read_position()
            #print(self._latitude)
            #print(self._longitude)
            if self.fml40_data_model[LATITUDE_PATH] != self._latitude:
                self.fml40_data_model[LATITUDE_PATH] = self._latitude
                self.sync_with_repo(LATITUDE_PATH, REPO_TOPIC)

            if self.fml40_data_model[LONGITUDE_PATH] != self._longitude:
                self.fml40_data_model[LONGITUDE_PATH] = self._longitude
                self.sync_with_repo(LONGITUDE_PATH, REPO_TOPIC)
            time.sleep(0.5)
