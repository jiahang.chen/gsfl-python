KARL_MODEL = {
    "thingId": "s3i:5a256363-4e77-4436-8c14-20b147b38819",
    "policyId": "s3i:5a256363-4e77-4436-8c14-20b147b38819",
    "attributes": {
        "class": "ml40::Thing",
        "name": "Waldarbeiter Karl",
        "roles": [{"class": "fml40::ForestWorker"}],
        "features": [
            {
                "class": "ml40::PersonalName",
                "firstName": "karl",
                "lastName": "Klammer"
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature: id1",
                "latitude": "ditto-feature: id2"
            },
            {
                "class": "fml40::AcceptsSingleTreeFellingJobs"
            },
            {
                "class": "AcceptsMoveCommands"
            },
            {
                "class": "ml40::Address",
                "country": "Germany",
                "streetnumber": 123,
                "city": "Arnsberg",
                "zip": 59755,
                "street": "Hauptstr."
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "longitude": 7.4063186
             }
        },
        "id2": {
            "properties": {
                "latitude": 51.4906236
            }
        }
    }
}

CLIENT_ID = "s3i:5a256363-4e77-4436-8c14-20b147b38819"
CLIENT_SECRET = "558cf99a-377e-4551-a099-1085decd5948"
USERNAME = "Karl"
PASSWORD = "ccODpRlgNkaOy1TQR3t7"
ENDPOINT = "s3ib://s3i:5a256363-4e77-4436-8c14-20b147b38819"
GRANT_TYPE = "password"
REPO_TOPIC = "s3i/5a256363-4e77-4436-8c14-20b147b38819/things/twin/commands/modify"
LONGITUDE_PATH = "features/id1/properties/longitude"
LATITUDE_PATH = "features/id2/properties/latitude"
