tree_species_classification_service = {
    "thingId": "s3i:897b771b-8a3c-4104-926e-fbdca73ad4a3",
    "policyId": "s3i:897b771b-8a3c-4104-926e-fbdca73ad4a3",
    "attributes": {
        "class": "ml40::Thing",
        "name": "tree species classification service",
        "roles": [{"class": "ml40::Service"}],
        "features": [
            {
                "class": "fml40::EvaluatesStandAttributes"
            },
            {
                "class": "fml40::ClassifiesTreeSpecies"
            }
        ]
    },
    "features": {

    }
}