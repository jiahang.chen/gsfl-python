HARVESTER_MODEL = {
    "thingId": "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63",
    "policyId": "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63",
    "attributes": {
        "class": "ml40::Thing",
        "name": "Harvester",
        "roles": [{"class": "fml40::Harvester"}],
        "features": [
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Motor",
                        "roles": [{"class": "ml40::Engine"}],
                        "features": [
                            {
                                "class": "ml40::RotationalSpeed",
                                "rpm": "ditto-feature:id1"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Kran",
                        "roles": [{"class": "ml40::Crane"}],
                        "features": [
                            {
                                "class": "ml40::Shared",
                                "targets": [
                                    {
                                        "class": "ml40::Thing",
                                        "name": "Harvesterkopf",
                                        "roles": "fml40::HarvestingHead",
                                        "features": [
                                            {
                                                "class": "fml40::Grabs"
                                            },
                                            {
                                                "class": "fml40::Cuts"
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Bordcomputer",
                        "roles": [{"class": "ml40::MachineUI"}]
                    }
                ]
            },
            {
                "class": "fml40::ProvidesProductionData"
            },
            {
                "class": "fml40::AcceptsFellingJobs"
            },
            {
                "class": "fml40::Harvests"
            },
            {
                "class": "ml40::ManagesJobs"
            },
            {
                "class": "fml40::AcceptsProximityAlert"
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature:id2",
                "latitude": "ditto-feature:id3"
            },
            {
                "class": "ml40::Dimensions",
                "width": 0.0,
                "height": 0.0,
                "weight": 0.0
            },
            {
                "class": "ml40::Weight",
                "weight": 0.0
            },
            {
                "class": "ml40::OperatingHours",
                "total": 12.3
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "rpm": 3000
            }
        },
        "id2": {
            "properties": {
                "longitude": 6.249276
            }
        },
        "id3": {
            "properties": {
                "latitude": 50.808797
            }
        }

    }
}
GRANT_TYPE = "client_credentials"
CLIENT_ID = "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"
CLIENT_SECRET = "950dc82f-1bac-4852-a3ee-1e62b22e258e"
ENDPOINT = "s3ib://" + "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"
MOTOR_RPM_PATH = "features/id1/properties/rpm"
LONGITUDE_PATH = "features/id2/properties/longitude"
LATITUDE_PATH = "features/id3/properties/latitude"
REPO_TOPIC = "s3i/ef39a0ae-1f4a-4393-9508-ad70a4d38a63/things/twin/commands/modify"
