from ..BaseThing import BaseThing
from s3i.messages import *
import uuid
from modelling_language.fml40 import AcceptsFellingJobs, ProvidesProductionData, Harvests, AcceptsProximityAlert
from modelling_language.ml40 import ManagesJobs
from .Config import *
import time
import serial


class Harvester(BaseThing):
    def __init__(self):
        super(Harvester, self).__init__(client_id=CLIENT_ID,
                                        client_secret=CLIENT_SECRET,
                                        endpoint=ENDPOINT,
                                        fml40_data_model=HARVESTER_MODEL,
                                        grant_type=GRANT_TYPE,
                                        is_broker=True,
                                        is_repo=True,
                                        is_gps=True,
                                        )
        AcceptsFellingJobs.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsFellingJobs").proxy()
        ProvidesProductionData.start(ref_managing_actor=self.actor_ref, name="fml40::ProvidesProductionData").proxy()
        Harvests.start(ref_managing_actor=self.actor_ref, name="fml40::Harvests").proxy()
        AcceptsProximityAlert.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsProximityAlert").proxy()
        ManagesJobs.start(ref_managing_actor=self.actor_ref, name="fml40::ManagesJobs").proxy()

    def on_service_request(self, msg: dict):
        service_reply = ServiceReply()
        request_sender = msg.get("sender")
        request_msg_id = msg.get("identifier")
        request_reply_endpoint = msg.get("replyToEndpoint")
        request_service_type = msg.get("serviceType")
        parameters = msg.get("parameters")
        reply_uuid = "s3i:" + str(uuid.uuid4())

        if request_service_type not in self.registeredServices:
            results = {"availableServiceType": self.registeredServices}

        else:
            if request_service_type == "fml40::AcceptsFellingJobs":
                job = parameters.get("job")
                accepted = self.proxyFunctionalities["fml40::AcceptsFellingJobs"].acceptJob(job)
                results = {"accepted": accepted.get()}

            elif request_service_type == "fml40::ProvidesProductionData":
                getData = self.proxyFunctionalities["fml40::ProvidesProductionData"].getProductionData()
                results = {"data": getData.get()}

            elif request_service_type == "fml40::Harvests":
                job = parameters.get("job")
                self.proxyFunctionalities["fml40::Harvests"].executeJob(job)
                results = {"executed": True}

            elif request_service_type == "fml40::AcceptsProximityAlert":
                self.proxyFunctionalities["fml40::AcceptsProximityAlert"].proximityAlert([1, 2], [1, 2])
                results = {"accepted": True}

            elif request_service_type == "fml40::ManagesJobs":
                job = parameters.get("job")
                self.proxyFunctionalities["fml40::ManagesJobs"].assignJob(job, str(uuid.uuid4()))
                results = {"managed": True}

        service_reply.fillServiceReply(senderUUID=CLIENT_ID,
                                       receiverUUID=request_sender,
                                       serviceType=request_service_type,
                                       results=results,
                                       msgUUID=reply_uuid,
                                       replyingToUUID=request_msg_id
                                       )
        msg = service_reply.msg.__str__()
        self._broker.send(receiver_endpoints=[request_reply_endpoint], msg=msg)

    def simulate_function(self, run=True):
        while run:
            self.read_position()
            if self.fml40_data_model[LATITUDE_PATH] != self._latitude:
                self.fml40_data_model[LATITUDE_PATH] = self._latitude
                self.sync_with_repo(LATITUDE_PATH, REPO_TOPIC)

            if self.fml40_data_model[LONGITUDE_PATH] != self._longitude:
                self.fml40_data_model[LONGITUDE_PATH] = self._longitude
                self.sync_with_repo(LONGITUDE_PATH, REPO_TOPIC)

            if self.fml40_data_model[MOTOR_RPM_PATH] > 0:
                __new_rpm = self.fml40_data_model[MOTOR_RPM_PATH] - 70
                self.fml40_data_model[MOTOR_RPM_PATH] = __new_rpm
            else:
                self.fml40_data_model[MOTOR_RPM_PATH] = 3000
            self.sync_with_repo(path=MOTOR_RPM_PATH, topic=REPO_TOPIC)
            time.sleep(0.1)
