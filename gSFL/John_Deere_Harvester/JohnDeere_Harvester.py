from ..BaseThing import BaseThing
from s3i.messages import *
import uuid
from .Config import *
from modelling_language.ml40 import ManagesJobs
from modelling_language.fml40 import AcceptsFellingJobs, ProvidesProductionData, Harvests, AcceptsProximityAlert
import time

class JohnDeereHarvester(BaseThing):
    def __init__(self):
        super().__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            endpoint=ENDPOINT,
            fml40_data_model=JOHN_DEERE_MODEL,
            grant_type=GRANT_TYPE,
            is_broker=True,
            is_repo=False)
        AcceptsFellingJobs.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsFellingJobs").proxy()
        ProvidesProductionData.start(ref_managing_actor=self.actor_ref, name="fml40::ProvidesProductionData").proxy()
        Harvests.start(ref_managing_actor=self.actor_ref, name="fml40::Harvests").proxy()
        AcceptsProximityAlert.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsProximityAlert").proxy()
        ManagesJobs.start(ref_managing_actor=self.actor_ref, name="ml40::ManagesJobs").proxy()

    def on_assign_forwarding_job(self, job):
        """
        Assgin a forwarding job to komatsu forwarder
        :param job:
        :return:
        """
        komatsu_id = "s3i:3154edfa-5b04-4a28-b803-d6ec46135c19"
        komatsu_endpoint = "s3ibs://{}".format(komatsu_id)
        service_type = "fml40::ForwardingJob"
        req = ServiceRequest()
        msg_uuid = "s3i://{}".format(uuid.uuid4())
        parameters = {"job": job}
        req.fillServiceRequest(CLIENT_ID, komatsu_id, ENDPOINT, service_type, parameters, msg_uuid)
        self._broker.send(receiver_endpoints=[komatsu_endpoint], msg=req.msg.__str__())

    def on_service_request(self, body_json):
        service_type = body_json.get("serviceType")
        if service_type == "fml40::FellingJob":
            accepted = self.proxyFunctionalities["fml40::AcceptsFellingJobs"].acceptJob("a fellingJob")
            if accepted.get():
                self.proxyFunctionalities["fml40::Harvests"].executeJob("a fellingJob")
                self.proxyFunctionalities["ml40::ManagesJobs"].assignJob("a forwardingJob", str(uuid.uuid4()))
