JOHN_DEERE_MODEL = {
    "thingId": "s3i:c5d5cd58-8786-40b2-8079-5f2de443de36",
    "policyId": "s3i:c5d5cd58-8786-40b2-8079-5f2de443de36",
    "attributes": {
        "class": "ml40::Thing",
        "name": "John Deere Harvester FBZ",
        "roles": [{"class": "fml40::Harvester"}],
        "features": [
            {"class": "fml40::AcceptsProximityAlert"},
            {"class": "fml40::AcceptsFellingJobs"},
            {"class": "fml40::Harvests"},
            {"class": "ml40::ManagesJobs"},
            {"class": "fml40::ProvidesProductionData"},
            {
                "class": "ml40::Location",
                "longitude": 6.2492276,
                "latitude": 50.808797
            },
            {
                "class": "ml40::Shared",
                "targets": ["s3i:..."]
            },
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "roles": ["ml40::Engine"],
                        "name": "Motor",
                        "identifier": "s3i:...",
                        "features": [{
                            "class": "ml40::RotationalSpeed",
                            "rpm": "3000"
                        }]
                    },
                    {
                        "class": "ml40::Thing",
                        "roles": ["ml40::Crane"],
                        "name": "Kran",
                        "identifier": "s3i:...",
                        "features": [{
                            "class": "ml40::Composite",
                            "targets": [{
                                "class": "ml40::Thing",
                                "roles": ["fml40::HarvestingHead"],
                                "name": "Harvesterkopf",
                                "identifier": "s3i:...",
                                "features": [{"class": "fml40::Grabs"}]
                            }]
                        }]
                    },
                    {
                        "class": "ml40::Thing",
                        "roles": ["ml40::MachineUI"],
                        "name": "Bordcomputer",
                        "identifier": "s3i:..."
                    }
                ]
            }
        ]
    }
}
CLIENT_ID = "s3i:c5d5cd58-8786-40b2-8079-5f2de443de36"
CLIENT_SECRET = "fc86fe53-1fb5-4cb7-96d2-c05c7e548d69"
GRANT_TYPE = "client_credentials"
ENDPOINT = "s3ibs://{}".format(CLIENT_ID)
REPO_TOPIC = "s3i/c5d5cd58-8786-40b2-8079-5f2de443de36/things/twin/commands/modify"
