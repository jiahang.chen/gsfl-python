from ..BaseThing import BaseThing
from s3i.messages import *
import uuid
from .Config import *
from modelling_language.ml40 import AcceptsJobs
from modelling_language.fml40 import TransportsLogs, AcceptsMoveCommands


class LKW(BaseThing):
    def __init__(self):
        super(LKW, self).__init__(client_id=CLIENT_ID,
                                  client_secret=CLIENT_SECRET,
                                  endpoint=ENDPOINT,
                                  fml40_data_model=LKW_MODEL,
                                  grant_type=GRANT_TYPE,
                                  is_broker=True,
                                  is_repo=True
        )

        AcceptsJobs.start(ref_managing_actor=self.actor_ref, name="ml40::AcceptsJobs").proxy()
        AcceptsMoveCommands.start(ref_managing_actor=self.actor_ref, name="fml40::AcceptsMoveCommands").proxy()
        TransportsLogs.start(ref_managing_actor=self.actor_ref, name="fml40::TransportsLogs").proxy()

    def on_service_request(self, msg: dict):
        service_reply = ServiceReply()
        request_sender = msg.get("sender")
        request_msg_id = msg.get("identifier")
        request_reply_endpoint = msg.get("replyToEndpoint")
        request_service_type = msg.get("serviceType")
        parameters = msg.get("parameters")
        reply_uuid = "s3i:" + str(uuid.uuid4())

        if request_service_type not in self.registeredServices:
            results = {"availableServiceType": self.registeredServices}

        elif request_service_type == "ml40::AcceptsJobs":
            job = parameters.get("job")
            accepted = self.proxyFunctionalities["ml40::AcceptsJobs"].acceptJob(job)
            results = {"accepted": accepted.get()}

        elif request_service_type == "fml40::AcceptsMoveCommands":
            direction = parameters.get("direction")
            speed = parameters.get("speed")
            self.proxyFunctionalities["fml40::AcceptsMoveCommands"].move(direction, speed)
            results = {"moved": True}

        elif request_service_type == "fml40::TransportsLogs":
            job = parameters.get("job")
            report = self.proxyFunctionalities["fml40::TransportsLogs"].transportLogs(job)
            results = {"logTransportationReport": report.get()}

        service_reply.fillServiceReply(senderUUID=CLIENT_ID,
                                       receiverUUID=request_sender,
                                       serviceType=request_service_type,
                                       results=results,
                                       msgUUID=reply_uuid,
                                       replyingToUUID=request_msg_id
                                       )
        msg = service_reply.msg.__str__()
        self._broker.send(receiver_endpoints=[request_reply_endpoint], msg=msg)


