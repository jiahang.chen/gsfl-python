LKW_MODEL = {
    "thingId": "s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c",
    "policyId": "s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c",
    "attributes": {
        "class": "ml40::Thing",
        "name": "gSFL LWK",
        "roles": [{"class": "fml40::LogTruck"}],
        "features": [
            {
                "class": "fml40::TransportLogs"
            },
            {
                "class": "ml40::AcceptsJobs"
            },
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Ladefläche",
                        "roles": [{"class": "fml40::LogLoadingArea"}],
                        "features": [
                            {
                                "class": "ml40::Weight",
                                "weight": 1000
                            }
                        ]
                    }
                ]
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature:id1",
                "latitude": "ditto-feature:id2"
            },
            {
                "class": "ml40::Weight",
                "weight": 0.0
            },
            {
                "class": "ml40::Dimensions",
                "length": 0.0,
                "width": 0.0,
                "height": 0.0
            },
            {
                "class": "fml40::AcceptsMoveCommands"
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "longitude": 6.0502
            }
        },
        "id2": {
            "properties": {
                "latitude": 50.4634
            }
        }
    }
}


CLIENT_ID = "s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c"
CLIENT_SECRET = "53427cd4-7248-492c-9f6f-a14e6d996059"
ENDPOINT = "s3ib://s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c"
LONGITUDE_PATH = "features/id1/properties/longitude"
LATITUDE_PATH = "features/id2/properties/latitude"
GRANT_TYPE = "client_credentials"
REPO_TOPIC = "s3i/9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c/things/twin/commands/modify"
