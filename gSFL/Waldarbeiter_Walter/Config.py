WALTER_MODEL = {
    "thingId": "s3i:0c253262-428e-44be-a11a-b83566bd1f68",
    "policyId": "s3i:0c253262-428e-44be-a11a-b83566bd1f68",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "fml40::ForestWorker"}],
        "name": "Walterarbeiter Walter",
        "features": [
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature: id1",
                "latitude": "ditto-feature: id2"
            },
            {
                "class": "ml40::PersonalName",
                "firstName": "Walter",
                "lastName": "Winkler"
            },
            {
                "class": "fml40::AcceptsSingleTreeFellingJobs"
            },
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "roles": [{"class": "fml40::VitalitySensor"}],
                        "features": [
                            {
                                "class": "fml40::VitalityStatus",
                                "ok": "ditto-feature: id3"
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "longitude": 7.4063
            }
        },
        "id2": {
            "properties": {
                "latitude": 51.490623
            }
        },
        "id3": {
            "properties": {
                "ok": True
            }
        }
    }
}


CLIENT_ID = "s3i:0c253262-428e-44be-a11a-b83566bd1f68"
CLIENT_SECRET = "8d9ad853-2bfb-44f1-ad47-bc2a6bad8a03"
USERNAME = "TODO"
PASSWORD = "TODO"
ENDPOINT = "s3ib://s3i:0c253262-428e-44be-a11a-b83566bd1f68"
GRANT_TYPE = "password"
REPO_TOPIC = "s3i/0c253262-428e-44be-a11a-b83566bd1f68/things/twin/commands/modify"
LONGITUDE_PATH = "features/id1/properties/longitude"
LATITUDE_PATH = "features/id2/properties/latitude"
VITA_STATUS_PATH = "features/id3/properties/ok"
