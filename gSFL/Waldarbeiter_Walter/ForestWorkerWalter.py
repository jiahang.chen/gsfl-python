from ..BaseThing import BaseThing
from s3i.messages import *
import uuid
from .Config import *
from modelling_language.fml40 import AcceptsSingleTreeFellingJobs


class ForestWorkerWalter(BaseThing):
    def __init__(self):
        username = USERNAME
        password = PASSWORD
        super(ForestWorkerWalter, self).__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            endpoint=ENDPOINT,
            fml40_data_model=WALTER_MODEL,
            grant_type=GRANT_TYPE,
            username=username,
            password=password,
            is_broker=True,
            is_repo=True
        )
        AcceptsSingleTreeFellingJobs.start(ref_managing_actor=self.actor_ref,
                                           name="fml40::AcceptsSingleTreeFellingJobs").proxy()

    def on_service_request(self, msg):
        service_reply = ServiceReply()
        request_sender = msg.get("sender")
        request_msg_id = msg.get("identifier")
        request_reply_endpoint = msg.get("replyToEndpoint")
        request_service_type = msg.get("serviceType")
        parameters = msg.get("parameters")
        reply_uuid = "s3i:{0}".format(str(uuid.uuid4()))
        if request_service_type not in self.registeredServices:
            results = {"availableServiceType": self.registeredServices}

        elif request_service_type == "fml40::AcceptsSingleTreeFellingJobs":
            job = parameters.get("job")
            self.proxyFunctionalities["fml40::AcceptsSingleTreeFellingJobs"].acceptJob(job)
            results = {"accepted": True}

        service_reply.fillServiceReply(senderUUID=CLIENT_ID,
                                       receiverUUID=request_sender,
                                       serviceType=request_service_type,
                                       results=results,
                                       msgUUID=reply_uuid,
                                       replyingToUUID=request_msg_id
                                       )
        self._broker.send(receiver_endpoints=[request_reply_endpoint], msg=service_reply.msg.__str__())

