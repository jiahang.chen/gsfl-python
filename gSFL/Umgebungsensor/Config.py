ENVIRONMENT_SENSOR_MODEL = {
    "thingId": "s3i:fa74c9ee-059b-430c-b672-59e52ce690f1",
    "policyId": "s3i:fa74c9ee-059b-430c-b672-59e52ce690f1",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "ml40::SoilSensor"}, {"class": "ml40::AirSensor"}],
        "features": [
            {
                "class": "ml40::Moisture",
                "humidity": "ditto-feature:id1"
            },
            {
                "class": "ml40::Temperature",
                "temperature": "ditto-feature:id2"
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "humidity": 0
            }
        },
        "id2": {
            "properties": {
                "temperature": 0
            }
        }
    }
}


GRANT_TYPE = "client_credentials"
CLIENT_ID = "s3i:fa74c9ee-059b-430c-b672-59e52ce690f1"
CLIENT_SECRET = "8ae3e6f5-a927-423c-af28-ab0ee1e0b6ae"
ENDPOINT = "s3ib://" + "s3i:fa74c9ee-059b-430c-b672-59e52ce690f1"
HUMIDITY_PATH = "features/id1/properties/humidity"
TEMPERATURE_PATH = "features/id2/properties/temperature"
REPO_TOPIC = "s3i/fa74c9ee-059b-430c-b672-59e52ce690f1/things/twin/commands/modify"
