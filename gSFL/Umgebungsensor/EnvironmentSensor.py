from ..BaseThing import BaseThing
import uuid
from s3i.messages import *
from .Config import *
import time
from modelling_language.ml40 import Temperature, Moisture


class EnvironmentSensor(BaseThing):
    def __init__(self):
        super(EnvironmentSensor, self).__init__(
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            endpoint=ENDPOINT,
            fml40_data_model=ENVIRONMENT_SENSOR_MODEL,
            grant_type=GRANT_TYPE,
            is_broker=True,
            is_repo=True
        )
        Temperature.start(ref_managing_actor=self.actor_ref, name="ml40::Temperature", temperature=0.000).proxy()
        Moisture.start(ref_managing_actor=self.actor_ref, name="ml40::Moisture", humidity=0.000).proxy()

    def simulate_function(self, run=True):
        while run:
            self.fml40_data_model[TEMPERATURE_PATH] = self.proxyFunctionalities["ml40::Temperature"].temperature.get()
            self.fml40_data_model[HUMIDITY_PATH] = self.proxyFunctionalities["ml40::Moisture"].humidity.get()
            self.proxyFunctionalities["ml40::Temperature"].temperature = \
                self.proxyFunctionalities["ml40::Temperature"].temperature.get() + 0.01
            self.proxyFunctionalities["ml40::Moisture"].humidity = \
                self.proxyFunctionalities["ml40::Moisture"].humidity.get() + 0.05
            self.sync_with_repo(TEMPERATURE_PATH, REPO_TOPIC)
            self.sync_with_repo(HUMIDITY_PATH, REPO_TOPIC)
            time.sleep(0.5)


