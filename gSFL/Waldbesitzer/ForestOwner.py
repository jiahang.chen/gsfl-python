forest_owner_date_model = {
    "thingId": "s3i:e61175c7-93dc-48f3-93f4-10aa186cc5e7",
    "policyId": "s3i:e61175c7-93dc-48f3-93f4-10aa186cc5e7",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "fml40::ForestOwner"}],
        "name": "DT of Forest Owner",
        "features": [
            {
                "class": "ml40::PersonalName",
                "firstName": "TODO",
                "lastName": "Maier"
            },
            {
                "class": "ml40::Location",
                "latitude": "ditto-feature:id1",
                "longitude": "ditto-feature:id2"
            },
            {
                "class": "ml40::Address",
                "streetnumber": "TODO",
                "city": "TODO",
                "zip": "TODO",
                "street": "TODO",
                "country": "TODO"
            }
        ]
    },
    "features": {
        "io1": {
            "properties": {
                "latitude": 8.052771
            }
        },
        "id2": {
            "properties": {
                "longitude": 51.407337
            }
        }
    }
}

