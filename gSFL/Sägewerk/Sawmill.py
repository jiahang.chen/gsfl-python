sawmill_data_model = {
    "thingId": "s3i:2073c475-fee5-463d-bce1-f702bb06f899",
    "policyId": "s3i:2073c475-fee5-463d-bce1-f702bb06f899",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "fml40::Sawmill"}],
        "name": "DT of Sawmill",
        "features": [
            {
                "class": "ml40::ManagesJobs"
            }

        ]
    },
    "features": {

    }
}