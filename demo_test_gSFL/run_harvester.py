import sys
sys.path.append("..")
from gSFL.Harvester import Harvester

if __name__ == "__main__":
    harvester = Harvester.start().proxy()
    harvester.run_forever()
