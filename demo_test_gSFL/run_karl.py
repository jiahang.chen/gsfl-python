import sys
sys.path.append("..")
from gSFL.Waldarbeiter_Karl import ForestWorkerKarl

if __name__ == "__main__":

    karl = ForestWorkerKarl.start().proxy()
    karl.run_forever()