from s3i.messages import *
from s3i.broker import *
from s3i.identity_provider import *
import uuid
import ast

CLIENT_ID = "s3i:da36496a-d2a8-411f-a01c-978e5e59d78a"
CLIENT_SECRET = "452d9a83-baa5-47bd-a01e-b437bfbe08f6"
ENDPOINT = "s3ib://s3i:da36496a-d2a8-411f-a01c-978e5e59d78a"

RECEIVER_ID = "s3i:f3302964-0110-424f-81b7-dc90127cd745"
RECEIVER_ENDPOINT = "s3ib://s3i:f3302964-0110-424f-81b7-dc90127cd745"

class bcolors:
    """colors for the console log"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def callback(ch, method, properties, body):
    body_json = ast.literal_eval(body.decode('utf-8'))
    message_type = body_json.get("messageType")
    print("[S³I]: we have got a {0}{1}{2}".format(bcolors.UNDERLINE, message_type, bcolors.ENDC))

    if message_type == "serviceReply":
        print("[S³I]: {0}{1}{2}".format(bcolors.OKGREEN, body_json.get("results"), bcolors.ENDC))
    elif message_type == "getValueReply":
        print("[S³I]: {0}{1}{2}".format(bcolors.OKGREEN, body_json.get("value"), bcolors.ENDC))
    ch.stop_consuming()


def prepare_service_request():
    available_services = ["fml40::Forwards"]
    print("[S³I]: Following services are available: {0}fml40::Forwards{1} ".format(bcolors.UNDERLINE, bcolors.ENDC))
    service_type = input("[S³I]: Please enter one of these services: ")

    if service_type not in available_services:
        print("[S³I]: service {0}{1}{2} is not available".format(bcolors.FAIL, service_type, bcolors.ENDC))
        return None
    elif service_type == "fml40::Forwards":
        job = input("[S³I]: Please enter your forwarding job: ")
        parameters = {"job": job}

    service_request = ServiceRequest()
    service_request.fillServiceRequest(senderUUID=CLIENT_ID,
                                       receiverUUID=RECEIVER_ENDPOINT,
                                       sender_endpoint=ENDPOINT,
                                       serviceType=service_type,
                                       parameters=parameters,
                                       msgUUID="s3i:" + str(uuid.uuid4())
                                       )
    return service_request.msg.__str__()


def prepare_get_value_request():
    attribute_path = input("[S³I]: Please enter a attribute path [e.g. features/id1/properties/longitude]: ")
    get_value_request = GetValueRequest()
    get_value_request.fillGetValueRequest(senderUUID=CLIENT_ID,
                                          receiverUUID=[RECEIVER_ENDPOINT],
                                          sender_endpoint=ENDPOINT,
                                          attributePath=attribute_path,
                                          msgUUID="s3i:" + str(uuid.uuid4()))
    return get_value_request.msg.__str__()



if __name__ == "__main__":

    print("[S³I]: {0} Welcome to demo client for Forwarder B gSFL {1}".format(bcolors.WARNING, bcolors.ENDC))

    idp = IdentityProvider(grant_type="client_credentials",
                           identity_provider_url="https://idp.s3i.vswf.dev/",
                           realm="KWH",
                           client_id=CLIENT_ID,
                           client_secret=CLIENT_SECRET
                           )

    access_token = idp.get_token(TokenType.ACCESS_TOKEN)
    broker = Broker(auth_form="Username/Password", username="",
                    password=access_token,
                    host="rabbitmq.s3i.vswf.dev")

    run = True

    while run:
        print("[S³I]: You can send following messages to forwarder a by entering 1 or 2")
        request_type = input(" {0}[1]: service request, [2]: get value request:{1} ".format(
            bcolors.OKBLUE, bcolors.ENDC
        ))

        if request_type == "1":
            msg = prepare_service_request()
            if msg is None:
                continue
        elif request_type == "2":
            msg = prepare_get_value_request()
            if msg is None:
                continue

        else:
            continue

        broker.send(receiver_endpoints=[RECEIVER_ENDPOINT], msg=msg)

        broker.receive(queue=ENDPOINT, callback=callback)