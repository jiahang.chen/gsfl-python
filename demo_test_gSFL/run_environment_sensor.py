import sys
sys.path.append("..")
from gSFL.Umgebungsensor import EnvironmentSensor

if __name__ == "__main__":

    sensor = EnvironmentSensor.start().proxy()
    sensor.run_forever()