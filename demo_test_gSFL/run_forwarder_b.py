import sys
sys.path.append("..")
from gSFL.Forwarder_B import ForwarderB

if __name__ == "__main__":
    forwarder_a = ForwarderB.start().proxy()
    forwarder_a.run_forever()
