import sys
sys.path.append("..")
from gSFL.Waldarbeiter_Walter import ForestWorkerWalter

if __name__ == "__main__":

    walter = ForestWorkerWalter.start().proxy()
    walter.run_forever()
