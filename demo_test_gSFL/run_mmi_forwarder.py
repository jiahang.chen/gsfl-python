import sys
sys.path.append("..")
from gSFL.MMI_Forwarder import MMIForwarder


if __name__ == "__main__":
    mmi_forwarder = MMIForwarder.start().proxy()
    mmi_forwarder.run_forever()
