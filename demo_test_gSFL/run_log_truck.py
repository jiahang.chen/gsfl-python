import sys
sys.path.append("..")
from gSFL.LKW import LKW

if __name__ == "__main__":
    log_truck = LKW.start().proxy()
    log_truck.run_forever()