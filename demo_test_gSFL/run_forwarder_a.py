import sys
sys.path.append("..")
from gSFL.Forwarder_A import ForwarderA

if __name__ == "__main__":
    forwarder_a = ForwarderA.start().proxy()
    forwarder_a.run_forever()
