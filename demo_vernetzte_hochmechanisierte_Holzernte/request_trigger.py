from s3i.messages import ServiceRequest
from s3i import IdentityProvider, TokenType, Broker
import uuid

data = {
    "thingId": "s3i:a8b7da14-9e43-47f0-a876-cfd1ca38a9e1",
    "policyId": "s3i:a8b7da14-9e43-47f0-a876-cfd1ca38a9e1",
    "attributes": {
        "class": "ml40::Thing",
        "name": "WZL-Demo Wald",
        "roles": [{"class": "fml40::Forest"}],
        "components": [
            {
                "class": "fml40::InventoryData",
                "data": {"class": "fml::AbstractInventory"}
            },
            "s3i:...",
            "s3i:..."
        ]
    }
}


if __name__ == "__main__":

    client_id = data.get("thingId")
    client_secret = "d48971ab-2c22-47d5-a8dc-86079a2c4cb6"
    johnDeere_id = "s3i:c5d5cd58-8786-40b2-8079-5f2de443de36"
    ponsse_id = "s3i:5e83b933-331f-4278-b318-b7fdcb0e4872"
    receiver_ids=[johnDeere_id, ponsse_id]
    idp = IdentityProvider(grant_type='client_credentials', identity_provider_url="https://idp.s3i.vswf.dev/",
                           realm='KWH', client_id=client_id, client_secret=client_secret)
    access_token = idp.get_token(TokenType.ACCESS_TOKEN)
    req = ServiceRequest()
    req.fillServiceRequest(
        senderUUID=client_id,
        receiverUUID=[johnDeere_id, ponsse_id],
        sender_endpoint="s3ib://{}".format(client_id),
        serviceType="fml40::FellingJob",
        parameters={},
        msgUUID="s3i:{}".format(uuid.uuid4())
    )
    broker = Broker(
        auth_form="Username/Password", username="",
        password=access_token, host="rabbitmq.s3i.vswf.dev"
    )
    receiver_endpoints=[("s3ibs://{}".format(i)) for i in receiver_ids]
    broker.send(receiver_endpoints, req.msg.__str__())