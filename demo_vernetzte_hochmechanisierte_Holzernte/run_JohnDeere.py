import sys
sys.path.append("..")
from gSFL.John_Deere_Harvester import JohnDeereHarvester


if __name__ == "__main__":
    john_deere_harvester = JohnDeereHarvester().start().proxy()
    john_deere_harvester.run_forever()