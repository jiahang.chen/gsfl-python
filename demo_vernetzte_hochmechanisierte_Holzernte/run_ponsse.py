import sys
sys.path.append("..")
from gSFL.Ponsse_Harvester import PonsseHarvester

if __name__ == "__main__":

    ponsse_harvester = PonsseHarvester.start().proxy()
    ponsse_harvester.run_forever()