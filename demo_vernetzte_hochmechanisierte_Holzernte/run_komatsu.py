import sys
sys.path.append("..")
from gSFL.Komatsu_Forwarder import KomatsuForwarder


if __name__ == "__main__":
    komatsu_forwarder = KomatsuForwarder().start().proxy()
    komatsu_forwarder.run_forever()