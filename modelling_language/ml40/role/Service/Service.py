from modelling_language.ml40.role.Role import Role


class Service(Role):
    def __init__(self, name, ref_managing_actor):
        super(Service, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor
        )
