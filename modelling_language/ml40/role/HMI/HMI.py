from modelling_language.ml40.role.Role import Role


class HMI(Role):
    def __init__(self, name, ref_managing_actor):
        super(HMI, self).__init__(name=name,
                                  ref_managing_actor=ref_managing_actor)
