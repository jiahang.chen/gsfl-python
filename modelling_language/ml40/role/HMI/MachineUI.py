from modelling_language.ml40.role.HMI import HMI


class MachineUI(HMI):
    def __init__(self, name, ref_managing_actor):
        super(MachineUI, self).__init__(name=name,
                                        ref_managing_actor=ref_managing_actor)
