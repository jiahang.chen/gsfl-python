from modelling_language.ml40.role.Role import Role


class DT(Role):
    def __init__(self, name, ref_managing_actor):
        super(DT, self).__init__(name=name,
                                 ref_managing_actor=ref_managing_actor)
