from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.ml40.feature.property.value.document.job import Job


class ClearsJobs(Functionality):
    def __init__(self, name, identifier, ref_managing_actor):
        super(ClearsJobs, self).__init__(name=name,
                                         identifier=identifier,
                                         ref_managing_actor=ref_managing_actor)

    def clear(self, job: Job) -> bool:
        pass