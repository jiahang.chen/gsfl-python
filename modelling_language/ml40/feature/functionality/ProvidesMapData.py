from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.report import PassabilityReport


class ProvidesMapData(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(ProvidesMapData, self).__init__(name=name,
                                              ref_managing_actor=ref_managing_actor)

    def getMapData(self, map: int) -> PassabilityReport:
        pass