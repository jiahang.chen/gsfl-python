from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.ml40.feature.property.value.document import Report, Job
from modelling_language.identifier import ID


class ManagesJobs(Functionality):
    def __init__(self, name, ref_managing_actor):
        super().__init__(name=name,
                         ref_managing_actor=ref_managing_actor)

    def acceptReport(self, report: Report):
        print("accepts Report ..")
        pass

    def assignJob(self, job: Job, identifier: ID):
        print("I assign the job {0} to Komatsu forwarder...".format(job))
        self.managing_actor.on_assign_forwarding_job(job)


