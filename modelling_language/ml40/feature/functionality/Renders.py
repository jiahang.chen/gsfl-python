from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.identifier import ID


class Renders(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(Renders, self).__init__(name=name,
                                      ref_managing_actor=ref_managing_actor)

    def create3DVideo(self, identifier: ID) -> bytes:
        pass

