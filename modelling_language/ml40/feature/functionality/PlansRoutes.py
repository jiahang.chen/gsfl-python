from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.ml40.feature.property import Route, Location


class PlansRoutes(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(PlansRoutes, self).__init__(name=name,
                                          ref_managing_actor=ref_managing_actor)

    def planRoute(self, start: Location, goal: Location) -> Route:
        pass
