from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.ml40.feature.property.value.document.job import Job, JobStatus
from modelling_language.identifier import ID


class AcceptsJobs(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(AcceptsJobs, self).__init__(
                                          name=name,
                                          ref_managing_actor=ref_managing_actor)
        self.__jobs = dict()

    def acceptJob(self, job: Job) -> bool:
        print("I am checking if i can accept job {}".format(job))
        return True

    def queryJobStatus(self, identifier: ID) -> JobStatus:
        pass

    def removeJob(self, identifier: ID) -> bool:
        pass