from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.ml40.feature.property.value.document.report import Report


class ProvidesOperationalData(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(ProvidesOperationalData, self).__init__(name=name,
                                                      ref_managing_actor=ref_managing_actor)

    def getOperationalData(self) -> Report:
        print("make the operational data report...")
        return "this is a simply report"
