from modelling_language.ml40.feature.Feature import Feature


class Property(Feature):
    def __init__(self, name, ref_managing_actor):
        super(Property, self).__init__(name=name,
                                       ref_managing_actor=ref_managing_actor)


