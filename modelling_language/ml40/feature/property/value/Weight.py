from modelling_language.ml40.feature.property.value import Value


class Weight(Value):
    def __init__(self, name, ref_managing_actor,
                 weight: float):
        super(Weight, self).__init__(name=name,
                                     ref_managing_actor=ref_managing_actor)
        self.weight = weight