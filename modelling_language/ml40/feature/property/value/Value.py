from modelling_language.ml40.feature.property import Property


class Value(Property):
    def __init__(self, name, ref_managing_actor):
        super(Value, self).__init__(name=name,
                                    ref_managing_actor=ref_managing_actor)

