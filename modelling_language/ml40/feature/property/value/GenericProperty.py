from modelling_language.ml40.feature.property.value import Value


class GenericProperty(Value):
    def __init__(self, name, ref_managing_actor, value: str):
        super(GenericProperty, self).__init__(name=name,
                                              ref_managing_actor=ref_managing_actor)
        self.value = value
        if not isinstance(value, str):
            raise TypeError("except str type")
