from modelling_language.ml40.feature.property.value import Value


class TimeSlot(Value):
    def __init__(self, name, ref_managing_actor,
                 end: int, start: int):
        super(TimeSlot, self).__init__(name=name,
                                       ref_managing_actor=ref_managing_actor)
        self.end = end
        self.start = start
