from modelling_language.ml40.feature.property.value import Value


class PersonalName(Value):
    def __init__(self, name, ref_managing_actor,
                 firstname: str, lastname:str):
        super(PersonalName, self).__init__(name=name,
                                           ref_managing_actor=ref_managing_actor)
        self.firstname = firstname
        self.lastname = lastname
