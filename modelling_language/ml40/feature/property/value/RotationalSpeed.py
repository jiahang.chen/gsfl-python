from modelling_language.ml40.feature.property.value import Value


class RotationalSpeed(Value):
    def __init__(self, name, ref_managing_actor, rpm):
        super(RotationalSpeed, self).__init__(name=name,
                                              ref_managing_actor=ref_managing_actor)
        self.rpm = rpm