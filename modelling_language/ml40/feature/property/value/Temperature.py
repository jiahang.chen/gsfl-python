from modelling_language.ml40.feature.property.value import Value


class Temperature(Value):
    def __init__(self, name, ref_managing_actor, temperature: float):
        super(Temperature, self).__init__(name=name,
                                          ref_managing_actor=ref_managing_actor)
        self.__temperature = temperature

    @property
    def temperature(self):
        return self.__temperature

    @temperature.setter
    def temperature(self, value):
        if isinstance(value, float):
            self.__temperature = value
        else:
            raise TypeError




