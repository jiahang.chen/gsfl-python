from modelling_language.ml40.feature.property.value import Value


class LiquidFillingLevel(Value):
    def __init__(self, name, ref_managing_actor,
                 currentLevel: float, maxLevel: float):
        super(LiquidFillingLevel, self).__init__(name=name,
                                                 ref_managing_actor=ref_managing_actor)
        self.currentLevel = currentLevel
        self.maxLevel = maxLevel
