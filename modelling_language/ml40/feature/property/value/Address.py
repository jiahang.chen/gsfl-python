from modelling_language.ml40.feature.property.value import Value


class Address(Value):
    def __init__(self, name, ref_managing_actor,
                 city: str, country: str, street: str, streetnumber: str, zip:str):
        super(Address, self).__init__(name=name,
                                      ref_managing_actor=ref_managing_actor)
        self.city = city
        self.street = street
        self.streetnumber = streetnumber
        self.zip = zip