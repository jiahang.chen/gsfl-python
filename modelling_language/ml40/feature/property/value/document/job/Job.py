from modelling_language.ml40.feature.property.value.document.Document import Document
from modelling_language.ml40.feature.property.value.document.job import JobStatus


class Job(Document):
    def __init__(self, name, ref_managing_actor, status: JobStatus):
        super(Job, self).__init__(name=name,
                                  ref_managing_actor=ref_managing_actor)
        self.status = status
