from modelling_language.ml40.feature.property.value import Value


class Document(Value):
    def __init__(self, name, ref_managing_actor):
        super(Document, self).__init__(name=name,
                                       ref_managing_actor=ref_managing_actor)
