from modelling_language.ml40.feature.property.value.document.Document import Document


class Report(Document):
    def __init__(self, name, ref_managing_actor):
        super(Report, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor
        )

