from modelling_language.ml40.feature.property.value import Value


class Dimensions(Value):
    def __init__(self, name, ref_managing_actor,
                 height: float, length: float, weight: float, width: float):
        super(Dimensions, self).__init__(name=name,
                                         ref_managing_actor=ref_managing_actor)
        self.height = height
        self.length = length
        self.weight = weight
        self.width = width
