from modelling_language.ml40.feature.property.value import Value


class Location(Value):
    def __init__(self, latitude: float, longitude: float, orientation: float):
        self.latitude = latitude
        self.longitude = longitude
        self.orientation = orientation

        if not isinstance(latitude, float) or not isinstance(longitude, float) or not isinstance(orientation, float):
            raise TypeError("except float")