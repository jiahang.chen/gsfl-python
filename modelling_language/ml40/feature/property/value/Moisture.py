from modelling_language.ml40.feature.property.value import Value


class Moisture(Value):
    def __init__(self, name, ref_managing_actor, humidity: float):
        super(Moisture, self).__init__(name=name,
                                       ref_managing_actor=ref_managing_actor)
        self.__humidity = humidity

    @property
    def humidity(self):
        return self.__humidity

    @humidity.setter
    def humidity(self, value):
        if isinstance(value, float):
            self.__humidity = value
        else:
            raise TypeError