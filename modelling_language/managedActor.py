import pykka
import logging
logging.basicConfig(level=logging.WARNING)


class ManagedActor(pykka.ThreadingActor):
    def __init__(self, ref_managing_actor, name):
        super(ManagedActor, self).__init__()
        self.managing_actor = ref_managing_actor.proxy()
        self.name = name
        self.managing_actor.registerManagedActor(name, self.actor_ref.proxy())
