from modelling_language.fml40.feature.property.value import AbstractInventory
from modelling_language.ml40.feature.property.value import Value


class InventoryData(Value):
    def __init__(self):
        self._data = []

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value: AbstractInventory):
        self._data.append(value)
