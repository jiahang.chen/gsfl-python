from modelling_language.ml40.feature.property.value import Value


class TreeType(Value):
    def __init__(self, name, ref_managing_ref, conifer: bool):
        super(TreeType, self).__init__(name=name,
                                       ref_managing_ref=ref_managing_ref)
        self.conifer = conifer