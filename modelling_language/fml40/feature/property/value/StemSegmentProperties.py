from modelling_language.ml40.feature.property.value import Value


class StemSegmentProperties(Value):
    def __init__(self, name, ref_managing_actor,
                 diameter: float, length: float, quality: str, woodType: str):
        super(StemSegmentProperties, self).__init__(name=name,
                                                    ref_managing_actor=ref_managing_actor)
        self.diameter = diameter
        self.length = length
        self.quality = quality
        self.woodType = woodType

