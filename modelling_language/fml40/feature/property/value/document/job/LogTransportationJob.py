from modelling_language.ml40.feature.property.value.document.job import Job


class LogTransportationJob(Job):
    def __init__(self, name, ref_managing_actor, woodPiles: list):
        super(LogTransportationJob, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor)
        self.woodPiles = woodPiles
