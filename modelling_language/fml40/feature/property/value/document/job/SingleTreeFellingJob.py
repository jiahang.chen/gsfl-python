from modelling_language.ml40.feature.property.value.document.job import Job
from modelling_language.identifier import ID


class SingleTreeFellingJob(Job):
    def __init__(self, name, ref_managing_actor, tree: ID):
        super(SingleTreeFellingJob, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor)
        self.tree = tree

