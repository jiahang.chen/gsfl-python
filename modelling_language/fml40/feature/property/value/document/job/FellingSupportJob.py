from modelling_language.ml40.feature.property.value.document.job import Job


class FellingSupportJob(Job):
    def __init__(self, name, ref_managing_actor):
        super(FellingSupportJob, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor)
