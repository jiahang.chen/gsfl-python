from modelling_language.ml40.feature.property.value.document.report import Report
from modelling_language.identifier import ID
from modelling_language.fml40.feature.property.value.document.report import FellingTool


class FellingMethodSuggestion(Report):
    def __init__(self, name, ref_managing_actor,
                 liftHeight: int, tool: FellingTool,
                 torque: float, tree: ID):
        self.liftHeight = liftHeight
        self.tool = tool
        self.torque = torque
        self.tree = tree
