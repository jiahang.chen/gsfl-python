from modelling_language.ml40.feature.property.value.document.report import Report


class AfforestationSuggestion(Report):
    def __init__(self, name, ref_managing_actor):
        super(AfforestationSuggestion, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor
        )
