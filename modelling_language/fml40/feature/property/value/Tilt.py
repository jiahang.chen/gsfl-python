from modelling_language.ml40.feature.property.value import Value


class Tilt(Value):
    def __init__(self, name, ref_managing_actor,
                 direction: float, tilt: float):
        super(Tilt, self).__init__(name=name,
                                   ref_managing_actor=ref_managing_actor)
        self.direction = direction
        self.tilt = tilt
