from modelling_language.ml40.feature.property.value import Value


class DBH(Value):
    def __init__(self, name, ref_managing_actor, dbh: float):
        super(DBH, self).__init__(name=name,
                                  ref_managing_actor=ref_managing_actor)
        self.dbh = dbh