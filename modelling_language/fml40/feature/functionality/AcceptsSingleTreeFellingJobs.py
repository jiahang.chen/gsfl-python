from modelling_language.ml40.feature.functionality import AcceptsJobs
from modelling_language.fml40.feature.property.value.document.job import SingleTreeFellingJob


class AcceptsSingleTreeFellingJobs(AcceptsJobs):
    def __init__(self, name, ref_managing_actor):
        super(AcceptsSingleTreeFellingJobs, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor)

    def acceptJob(self, job: SingleTreeFellingJob) -> bool:
        print("i am doing job {}".format(job))
        return True
