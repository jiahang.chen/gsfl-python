from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.job import FellingJob


class Harvests(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(Harvests, self).__init__(name=name,
                                       ref_managing_actor=ref_managing_actor)

    def executeJob(self, job: FellingJob):
        print("I am executing the harvesting Job {}".format(job))
        pass

