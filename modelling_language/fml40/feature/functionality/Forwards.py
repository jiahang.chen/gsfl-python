from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.job import ForwardingJob


class Forwards(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(Forwards, self).__init__(name=name,
                                       ref_managing_actor=ref_managing_actor)

    def executeJob(self, job: ForwardingJob):
        print("I am executing the forwarding Job {}".format(job))
        pass

