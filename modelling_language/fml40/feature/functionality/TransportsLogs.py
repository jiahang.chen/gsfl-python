from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.job import LogTransportationJob
from modelling_language.fml40.feature.property.value.document.report import LogTransportationReport


class TransportsLogs(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(TransportsLogs, self).__init__(
            name=name,
            ref_managing_actor=ref_managing_actor)

    def transportLogs(self, job: LogTransportationJob) -> LogTransportationReport:
        print("i am making Log transportation report for the job {}".format(job))
        return "the transportation report is huge"
