from modelling_language.ml40.feature.functionality import AcceptsJobs
from modelling_language.fml40.feature.property.value.document.job import FellingJob


class AcceptsFellingJobs(AcceptsJobs):
    def __init__(self, name, ref_managing_actor):
        super(AcceptsFellingJobs, self).__init__(name=name,
                                                 ref_managing_actor=ref_managing_actor)

    def acceptJob(self, job: FellingJob) -> bool:
        print("I check if I can accept the Job {}".format(job))
        return True
