from modelling_language.ml40.feature.functionality import Functionality


class AcceptsShieldCommands(Functionality):
    def __init__(self, name, ref_managing_actor):
        super().__init__(
            name=name,
            ref_managing_actor=ref_managing_actor
        )

    def shield_up(self):
        print("i am raising the shield up")

    def shield_down(self):
        print("i am sinking the shield down")
