from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.report import SoilMoistureMeasurement


class AcceptsMoistureMeasurement(Functionality):
    def acceptMoistureMeasurement(self, input: SoilMoistureMeasurement):
        pass