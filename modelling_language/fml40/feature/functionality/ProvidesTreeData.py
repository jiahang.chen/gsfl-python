from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.ml40.feature.property.value import Location
from modelling_language.identifier import ID
from modelling_language.fml40.feature.property.value import TreeData


class ProvidesTreeData(Functionality):

    def getTreeData(self, Tree:ID) -> list:
        pass

    def getTreesInDiameter(self, location: Location, diameter: float) -> list:
        pass
