from modelling_language.ml40.feature.functionality import Functionality
from datetime import date


class EvaluatesStandAttributes(Functionality):
    def calculateStandAttributes(self, input_a: bytes, input_b: date) -> str:
        pass

    def calculateStock(self, input_a: bytes) -> float:
        pass
