from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.identifier import ID
from modelling_language.fml40.feature.property.value.document.report import PassabilityReport


class ProvidesPassabilityInformation(Functionality):
    def getPassability(self, id:ID) -> PassabilityReport:
        pass