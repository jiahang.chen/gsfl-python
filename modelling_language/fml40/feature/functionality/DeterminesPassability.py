from modelling_language.ml40.feature.functionality import Functionality
from datetime import date


class DeterminesPassability(Functionality):

    def isPassableNow(self, input: float) -> bool:
        pass

    def maxPassableWeightNow(self) -> float:
        pass

    def maxPassableWeightOnDate(self, date: date) -> float:
        pass

    def willBePassable(self, input: float) -> date:
        pass
