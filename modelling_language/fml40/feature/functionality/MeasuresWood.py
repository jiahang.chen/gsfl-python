from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.report import LogMeasurement


class MeasuresWood(Functionality):
    def __init__(self):
        pass

    def measureLog(self) -> LogMeasurement:
        pass
