from modelling_language.ml40.feature.functionality import Functionality


class AcceptsMoveCommands(Functionality):
    def __init__(self, name, ref_managing_actor):
        super().__init__(
            name=name,
            ref_managing_actor=ref_managing_actor
        )

    def move(self, direction: float, speed: float):
        print("i am moving with speed {} to direction {}".format(speed, direction))

