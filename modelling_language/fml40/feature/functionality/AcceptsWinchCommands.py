from modelling_language.ml40.feature.functionality import Functionality


class AcceptsWinchCommands(Functionality):
    def __init__(self, name, ref_managing_actor):
        super().__init__(
            name=name,
            ref_managing_actor=ref_managing_actor
        )

    def roll_up(self):
        print("i am rolling the winch up")

    def roll_down(self):
        print("i am rolling the winch down")