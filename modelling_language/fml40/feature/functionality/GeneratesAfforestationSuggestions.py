from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.report import AfforestationSuggestion


class GeneratesAfforestationSuggestions(Functionality):

    def generateAfforestationSuggestion(self) -> AfforestationSuggestion:
        pass