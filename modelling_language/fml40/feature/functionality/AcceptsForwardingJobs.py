from modelling_language.ml40.feature.functionality import AcceptsJobs
from modelling_language.fml40.feature.property.value.document.job import ForwardingJob


class AcceptsForwardingJobs(AcceptsJobs):

    def __init__(self, ref_managing_actor, name):
        super().__init__(ref_managing_actor=ref_managing_actor,
                         name=name)

    def acceptJob(self, job: ForwardingJob) -> bool:
        print("I check if i can accept the job {}".format(job))
        return True
