from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.identifier import ID


class AcceptsProximityAlert(Functionality):
    def __init__(self, name, ref_managing_actor):
        super().__init__(
            name=name,
            ref_managing_actor=ref_managing_actor)

    def proximityAlert(self, ids: list, distances: list):
        print("Making Proximity Alert...")

