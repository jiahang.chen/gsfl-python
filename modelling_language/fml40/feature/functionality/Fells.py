from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value import TreeData
from modelling_language.fml40.feature.property.value.document.job import FellingJob

class Fells(Functionality):

    def __init__(self, name, ref_managing_actor):
        super().__init__(name=name, ref_managing_actor=ref_managing_actor)

    def executeFellingJob(self, job: FellingJob):
        print("i am executing the felling job {}".format(job))
        pass

    def fell(self, tree_data: TreeData):
        print("i am felling the tree {}".format(tree_data))
        pass
