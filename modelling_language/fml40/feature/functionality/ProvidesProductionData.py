from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.report import ProductionData


class ProvidesProductionData(Functionality):
    def __init__(self, name, ref_managing_actor):
        super(ProvidesProductionData, self).__init__(name=name,
                                                     ref_managing_actor=ref_managing_actor)

    def getProductionData(self) -> ProductionData:
        return "Production Data is huge"