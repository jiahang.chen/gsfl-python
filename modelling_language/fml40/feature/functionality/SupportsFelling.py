from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.job import FellingJob
from modelling_language.fml40.feature.property.value.document.report import FellingSupportSuggestion


class SupportsFelling(Functionality):

    def supportFelling(self, job:FellingJob, suggestion: FellingSupportSuggestion):
        pass
