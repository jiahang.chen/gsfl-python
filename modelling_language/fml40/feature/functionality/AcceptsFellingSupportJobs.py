from modelling_language.ml40.feature.functionality.AcceptsJob import AcceptsJobs
from modelling_language.fml40.feature.property.value.document.job.FellingSupportJob import FellingSupportJob


class AcceptFellingSupportJobs(AcceptsJobs):

    def acceptJob(self, job: FellingSupportJob) -> bool:
        pass
