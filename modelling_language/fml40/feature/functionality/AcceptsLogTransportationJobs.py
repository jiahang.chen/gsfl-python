from modelling_language.ml40.feature.functionality import AcceptsJobs
from modelling_language.fml40.feature.property.value.document.job import LogTransportationJob


class AcceptsLogTransportationJobs(AcceptsJobs):

    def acceptJob(self, job: LogTransportationJob) -> bool:
        pass
