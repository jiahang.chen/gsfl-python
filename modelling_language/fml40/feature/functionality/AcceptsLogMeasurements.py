from modelling_language.ml40.feature.functionality import Functionality
from modelling_language.fml40.feature.property.value.document.report import LogMeasurement


class AcceptsLogMeasurements(Functionality):

    def acceptLogMeasurement(self, log_measurement: LogMeasurement) -> bool:
        pass
