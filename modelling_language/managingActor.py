import pykka
import logging
logging.basicConfig(level=logging.WARNING)


class ManagingActor(pykka.ThreadingActor):
    def __init__(self):
        super().__init__()
        self.proxyFunctionalities = dict()
        self.registeredServices = list()

    def on_receive(self, message):
        print("get a message:", message)

    def registerManagedActor(self, name, proxy):
        self.proxyFunctionalities[name] = proxy
        self.registeredServices.append(name)

    def on_stop(self):
        for i in self.proxyFunctionalities.keys():
            pykka.ActorRegistry.get_by_class_name(i)[0].stop()