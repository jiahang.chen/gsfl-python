
class ID(object):
    """
    Define primitive variable type ID
    """
    def __init__(self, identifier):
        self.__identifier = identifier

    @property
    def ID(self):
        return self.__identifier

    @ID.setter
    def ID(self, value):
        """
        TODO specification ID e.g. s3i:uuid or gSFL:uuid?
        :param value: identifier
        :return: identifier
        """
        self.__identifier = value
